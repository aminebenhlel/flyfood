<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiProductDetailsController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "produit";        
				$this->permalink   = "product_details";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if($result){
					$groups= DB::table('produit_options_groupe')
						->where('id_produit', $postdata['id'])
						->join('options_groupe', 'options_groupe.id', '=', 'produit_options_groupe.id_options_groupe')
						->select('options_groupe.id', 'produit_options_groupe.min', 'produit_options_groupe.max', 'options_groupe.name')
						->get();

					$result['options_groupes']=$groups->toArray();
					
					foreach ($result['options_groupes'] as $key => $value) {
						$options= DB::table('produit_options')
							->where('id_produit', $postdata['id'])
							->join('options', 'options.id', '=', 'produit_options.id_options')
							->where('options.id_options_groupe', $value->id)
							->select('options.id', 'options.name', 'options.prix')
							->get();
						
						$result['options_groupes'][$key]->options=$options->toArray();
					}

					$supplements= DB::table('produit_supplement')
						->where('id_produit', $postdata['id'])
						->join('supplement', 'supplement.id', '=', 'produit_supplement.id_supplement')
						->select('supplement.id', 'supplement.name', 'supplement.prix')
						->get();
					
					$result['supplements']=$supplements->toArray();

					$ingredients= DB::table('produit_ingredient')
						->where('id_produit', $postdata['id'])
						->join('ingredient', 'ingredient.id', '=', 'produit_ingredient.id_ingredient')
						->select('ingredient.id', 'ingredient.name')
						->get();
					
					$result['ingredients']=$ingredients->toArray();

					if(DB::table('produit_favouris')->where('id_produit', $postdata['id'])->where('id_client', $postdata['id_client'])->exists()){
						$result['is_favourite'] = 1;
					}
					else{
						$result['is_favourite'] = 0;
					}
				}
		    }

		}