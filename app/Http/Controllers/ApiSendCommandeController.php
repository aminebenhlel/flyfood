<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiSendCommandeController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "commande";        
				$this->permalink   = "send_commande";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if($postdata){
					$commande_id= DB::table('commande')
									->insertGetId([
										'id_client' => $postdata['id_client'],
										'prix' => $postdata['total'],
										'adresse' => $postdata['adresse']
									]);
									
					foreach ($postdata['panier'] as $produit) {
						$id_produit = DB::table('commande_produit')
										->insertGetId([
											'id_produit' => $produit['id'],
											'id_commande' => $commande_id,
											'prix' => $produit['prix'],
											'quantite' => $produit['quantite']
										]);

						if($produit['options_groupe']){
							foreach ($produit['options_groupe'] as $option_groupe) {
								if($option_groupe['options']){
									foreach ($option_groupe['options'] as $option) {
										DB::table('commande_produit_options')
											->insert([
												'id_options' => $option,
												'id_commande_produit' => $id_produit
											]);
									}
								}
							}
						}
						if($produit['supplements']){
							foreach ($produit['supplements'] as $supplement) {
								DB::table('commande_produit_supplement')
									->insert([
										'id_supplement' => $supplement,
										'id_commande_produit' => $id_produit
									]);
							}
						}
						if($produit['ingredients']){
							foreach ($produit['ingredients'] as $ingredient) {
								DB::table('commande_produit_ingredient')
									->insert([
										'id_ingredient' => $ingredient,
										'id_commande_produit' => $id_produit
									]);
							}
						}
					}
					$config = [];
					$config['content'] = "Nouvelle commande : (".date("H:i",time()+60*60).")";
					$config['to'] = CRUDBooster::adminPath('')."/commande/detail/".$commande_id;
					$config['id_cms_users'] = [2,4,6,7];
					CRUDBooster::sendNotification($config);
					$result['id_commande'] = $commande_id;
				}
		    }

		}