<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiCategoriesListingController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "produit_categorie";        
				$this->permalink   = "categories_listing";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if($result['data']){
					foreach ($result['data'] as $i => $value) {
						if($value->name=='Autre'){
							unset($result['data'][$i]);
						}
					}
					$result['data']= $result['data']->values()->all();
				}
		    }

		}