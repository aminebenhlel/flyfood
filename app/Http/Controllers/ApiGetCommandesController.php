<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiGetCommandesController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "commande";        
				$this->permalink   = "get_commandes";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if($postdata['id_client']){
					foreach ($result['data'] as  $commande) {
						$products = DB::table('commande_produit')
									->where('id_commande', $commande->id)
									->join('produit', 'produit.id', '=', 'commande_produit.id_produit')
									->select('produit.name', 'commande_produit.prix', 'commande_produit.quantite', 'produit.photo')
									->get();
						$commande->produit = $products;
						$id_client = $postdata['id_client'];
					}
				}
		    }

		}