<!-- <?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use BaseController;
	use File;

	class AdminRestoController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "restaurant";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Name","name"=>"name"];
			$this->col[] = ["label"=>"Photo","name"=>"photo","image"=>true];
			$this->col[] = ["label"=>"cover_photo","name"=>"Cover Photo","image"=>true];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Propriétaire','name'=>'id_cms_users','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','validation'=>'required|max:3000','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			$this->form[] = ['label'=>'Cover Photo','name'=>'cover_photo','type'=>'upload','validation'=>'required|max:3000','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			$this->form[] = ['label'=>'Lundi','name'=>'is_closed_day1','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Mardi','name'=>'is_closed_day2','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Mercredi','name'=>'is_closed_day3','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Jeudi','name'=>'is_closed_day4','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Vendredi','name'=>'is_closed_day5','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Samedi','name'=>'is_closed_day6','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Dimanche','name'=>'is_closed_day7','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			$this->form[] = ['label'=>'Open At','name'=>'open_at','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Close At','name'=>'close_at','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Propriétaire','name'=>'id_cms_users','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','validation'=>'required|max:3000','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			//$this->form[] = ['label'=>'Lundi','name'=>'is_closed_day1','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Mardi','name'=>'is_closed_day2','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Mercredi','name'=>'is_closed_day3','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Jeudi','name'=>'is_closed_day4','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Vendredi','name'=>'is_closed_day5','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Samedi','name'=>'is_closed_day6','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Dimanche','name'=>'is_closed_day7','type'=>'radio','validation'=>'required|integer','width'=>'col-sm-10','dataenum'=>'1|Fermé;0|Ouvert'];
			//$this->form[] = ['label'=>'Open At','name'=>'open_at','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Close At','name'=>'close_at','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        
			$produit_id = DB::table('produit')
				->where('id_restaurant', $id)
				->select('id')
				->get();

			$photo = DB::table('restaurant')
				->where('id', $id)
				->select('photo')
				->first();
			$photo = $photo->photo;

			//dd($photo);

			//dd(File::exists(public_path($photo)));

			if (File::exists(public_path('uploads/csv/img.png'))) {
				File::delete(public_path('uploads/csv/img.png'));
			}

			foreach ($produit_id as $id_p) {
				DB::table('produit_supplement')
						->where('id_produit', $id_p->id)
						->delete();
						
				DB::table('produit_ingredient')
						->where('id_produit', $id_p->id)
						->delete();
		
				DB::table('produit_options_groupe')
						->where('id_produit', $id_p->id)
						->delete();
				
				DB::table('produit_options')
						->where('id_produit', $id_p->id)
						->delete();
				
				DB::table('produit')
						->where('id', $id_p->id)
						->delete();
				
			}
				

			DB::table('supplement')
					->where('id_restaurant', $id)
					->delete();

			DB::table('ingredient')
					->where('id_restaurant', $id)
					->delete();

			$options_groupe_id=DB::table('options_groupe')
					->where('id_restaurant', $id)
					->select('id')
					->get();

			foreach ($options_groupe_id as $id_g) {
				DB::table('options')
						->where('id_options_groupe', $id_g->id)
						->delete();
			}

			DB::table('options_groupe')
					->where('id_restaurant', $id)
					->delete();

			

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

		public function getAdd() {
			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$Proprietaire = DB::table('cms_users')
				->select('id','name')
				->get();

			$categories = DB::table('produit_categorie')
				->select('id','name')
				->get();
			
				//dd($Proprietaire, $categories);

			$data = [];
			$data['page_title'] = 'Add Restaurant';
			$data['users'] = $Proprietaire;
			$data['categories'] = $categories;
			
			return view('addRestaurant',$data);
		}
		
		public function addRestaurant(request $request) {
			$name = $request->name;
			$restaurant_Proprietaire = $request->restaurant_Proprietaire;
			$photo = $request->photo;
			$cover_photo= $request->cover_photo;
			$jours[] = $request->jours;
			$open_at = $request->open_at;
			$close_at = $request->close_at;
			$supplements_name = $request->supplements_name;
			$supplements_prix = $request->supplements_prix;
			$ingredients = $request->ingredients;
			$categories = $request->categories;
			if($request->options){
				for($i=0;$i<count($request->options);$i++){
					$options[$i] = json_decode($request->options[$i]);
				}
			}
			if(in_array('1',$jours)){
				$is_closed_day1 = 1;
			}
			else{
				$is_closed_day1 = 0;
			}
			if(in_array('2',$jours)){
				$is_closed_day2 = 1;
			}
			else{
				$is_closed_day2 = 0;
			}
			if(in_array('3',$jours)){
				$is_closed_day3 = 1;
			}
			else{
				$is_closed_day3 = 0;
			}
			if(in_array('4',$jours)){
				$is_closed_day4 = 1;
			}
			else{
				$is_closed_day4 = 0;
			}
			if(in_array('5',$jours)){
				$is_closed_day5 = 1;
			}
			else{
				$is_closed_day5 = 0;
			}
			if(in_array('6',$jours)){
				$is_closed_day6 = 1;
			}
			else{
				$is_closed_day6 = 0;
			}
			if(in_array('7',$jours)){
				$is_closed_day7 = 1;
			}
			else{
				$is_closed_day7 = 0;
			}
			
			$id=DB::table('restaurant')
				->insertGetId([
					"id_cms_users" => $restaurant_Proprietaire,
					"name" => $name,
					'is_closed_day1' => $is_closed_day1,
					'is_closed_day2' => $is_closed_day2,
					'is_closed_day3' => $is_closed_day3,
					'is_closed_day4' => $is_closed_day4,
					'is_closed_day5' => $is_closed_day5,
					'is_closed_day6' => $is_closed_day6,
					'is_closed_day7' => $is_closed_day7,
					'open_at' => $open_at,
					'close_at' => $close_at
				]);

			
			if($photo){
				$photo->move(public_path('/uploads'), time().'_'.$request->file('photo')->getClientOriginalName());
				DB::table('restaurant')
					->where('id',$id)
					->update(['photo' => '/uploads/'.time().'_'.$request->file('photo')->getClientOriginalName()]);
			}

			if($cover_photo){
				$cover_photo->move(public_path('/uploads'), time().'_'.$request->file('cover_photo')->getClientOriginalName());
				DB::table('restaurant')
					->where('id',$id)
					->update(['cover_photo' => '/uploads/'.time().'_'.$request->file('cover_photo')->getClientOriginalName()]);
			}
			
			if($categories){
				foreach ($categories as $value) {

					DB::table('restaurant_categories')
						->insert(
							[
								'id_restaurant' => $id,
								'id_categories' => $value
							]
							);
				}
			}

			if($supplements_name){
				for($i=0;$i<count($supplements_name);$i++){
					DB::table('supplement')
						->where("id_restaurant",$id)
						->insert([
							'id_restaurant' => $id,
							'name' => $supplements_name[$i],
							'prix' => $supplements_prix[$i]
						]);
				}
			}

			if($ingredients){
				for($i=0;$i<count($ingredients);$i++){
					DB::table('ingredient')
						->where("id_restaurant",$id)
						->insert([
							'id_restaurant' => $id,
							'name' => $ingredients[$i],
						]);
				}
			}

			if($options){
				foreach ($options as $option) {
					$groupe_id = DB::table('options_groupe')
						->insertGetId([
							'id_restaurant' => $id,
							'name' => $option->name,
							'min' => $option->min,
							'max' => $option->max,
						]);
				
					for($i=0;$i<count($option->options);$i++){
						DB::table('options')
						->insert([
							'id_options_groupe' => $groupe_id,
							'name' => $option->options[$i]->name,
							'prix' => $option->options[$i]->prix,
						]);
						$options_id[$i] = $option_id;
					}
				}
			}	
			

			CRUDBooster::redirect("/admin/Restaurant/detail/".$id,"Le restaurant ".$name." a été ajouté avec succès !","success");
			
		}

		public function getDetail($id) {
			if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$restaurant = DB::table('restaurant')
				->select('name','photo','cover_photo','is_closed_day1','is_closed_day2','is_closed_day3','is_closed_day4','is_closed_day5','is_closed_day6','is_closed_day7','open_at','close_at')
				->where('id' , $id)
				->first();

			$restaurant_Proprietaire = DB::table('cms_users')
				->where('restaurant.id' , $id)
				->join('restaurant','restaurant.id_cms_users','=','cms_users.id')
				->select('cms_users.name')
				->first();

			$produits = DB::table('produit')
				->select('name','prix')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();

			$supplements = DB::table('supplement')
				->select('id','name','prix')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();
			
			$ingredients = DB::table('ingredient')
				->select('id','name')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();
			
			$options_groupe = DB::table('options_groupe')
				->select('id','name','min','max')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();

			foreach ($options_groupe as $groupe) {
				$groupe->options = DB::table('options')
									->select('id','name','prix')
									->where('id_options_groupe', $groupe->id )
									->orderBy('prix', 'ASC')
									->get();
			}
			
			$data = [];
			$data['page_title'] = 'Restaurant : '.$restaurant->name;
			$data['restaurant'] = $restaurant;
			$data['produits'] = $produits;
			$data['supplements'] = $supplements;
			$data['ingredients'] = $ingredients;
			$data['options_groupe'] = $options_groupe;
			$data['restaurant_Proprietaire'] = $restaurant_Proprietaire;
			
			return view('viewRestaurant',$data);
		}

		public function getEdit($id) {
			if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$restaurant = DB::table('restaurant')
				->select('id','name','photo','cover_photo','is_closed_day1','is_closed_day2','is_closed_day3','is_closed_day4','is_closed_day5','is_closed_day6','is_closed_day7','open_at','close_at')
				->where('id' , $id)
				->first();

			$restaurant_Proprietaire = DB::table('cms_users')
				->where('restaurant.id' , $id)
				->join('restaurant','restaurant.id_cms_users','=','cms_users.id')
				->select('cms_users.name')
				->first();

			$produits = DB::table('produit')
				->select('name','prix')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();

			$supplements = DB::table('supplement')
				->select('id','name','prix')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();
			
			$ingredients = DB::table('ingredient')
				->select('id','name')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();
			
			$options_groupe = DB::table('options_groupe')
				->select('id','name','min','max')
				->where('id_restaurant',$id)
				->orderBy('name', 'ASC')
				->get();

			foreach ($options_groupe as $groupe) {
				$groupe->options = DB::table('options')
									->select('id','name','prix')
									->where('id_options_groupe', $groupe->id )
									->orderBy('prix', 'ASC')
									->get();
			}

			$users = DB::table('cms_users')
				->select('name','id')
				->get();
			
			$data = [];
			$data['page_title'] = 'Restaurant : '.$restaurant->name;
			$data['restaurant'] = $restaurant;
			$data['produits'] = $produits;
			$data['supplements'] = $supplements;
			$data['ingredients'] = $ingredients;
			$data['options_groupe'] = $options_groupe;
			$data['restaurant_Proprietaire'] = $restaurant_Proprietaire;
			$data['users'] = $users;
			
			return view('editRestaurant',$data);
		}

		public function editRestaurant(request $request){
			//dd($request->request);
			//dd(time().'_'.$request->file('photo')->getClientOriginalName());
			//Input::file('uploadfile')->move(public_path().'/upload',Input::file('uploadfile')->getClientOriginalName());
			$id = $request->id_restaurant;
			$name = $request->name;
			$proprietaire = $request->restaurant_Proprietaire;
			$jours = $request->jours;
			$open_at = $request->open_at;
			$close_at = $request->close_at;
			$photo = $request->file('photo');
			$cover_photo = $request->file('cover_photo');
			if($photo){
				$photo->move(public_path('/uploads'), time().'_'.$request->file('photo')->getClientOriginalName());
				DB::table('restaurant')
					->where('id',$id)
					->update([
								'photo' => '/uploads/'.time().'_'.$request->file('photo')->getClientOriginalName(),
							]);
			}

			if($cover_photo){
				$cover_photo->move(public_path('/uploads'), time().'_'.$request->file('cover_photo')->getClientOriginalName());
				DB::table('restaurant')
					->where('id',$id)
					->update([
								'cover_photo' => '/uploads/'.time().'_'.$request->file('cover_photo')->getClientOriginalName(),
							]);
			}

			if(in_array('1',$jours)){
				$is_closed_day1 = 1;
			}
			else{
				$is_closed_day1 = 0;
			}
			if(in_array('2',$jours)){
				$is_closed_day2 = 1;
			}
			else{
				$is_closed_day2 = 0;
			}
			if(in_array('3',$jours)){
				$is_closed_day3 = 1;
			}
			else{
				$is_closed_day3 = 0;
			}
			if(in_array('4',$jours)){
				$is_closed_day4 = 1;
			}
			else{
				$is_closed_day4 = 0;
			}
			if(in_array('5',$jours)){
				$is_closed_day5 = 1;
			}
			else{
				$is_closed_day5 = 0;
			}
			if(in_array('6',$jours)){
				$is_closed_day6 = 1;
			}
			else{
				$is_closed_day6 = 0;
			}
			if(in_array('7',$jours)){
				$is_closed_day7 = 1;
			}
			else{
				$is_closed_day7 = 0;
			}
			DB::table('restaurant')
				->where('id',$id)
				->update([
							'id_cms_users' => $proprietaire,
							'name' => $name,
							'is_closed_day1' => $is_closed_day1,
							'is_closed_day2' => $is_closed_day2,
							'is_closed_day3' => $is_closed_day3,
							'is_closed_day4' => $is_closed_day4,
							'is_closed_day5' => $is_closed_day5,
							'is_closed_day6' => $is_closed_day6,
							'is_closed_day7' => $is_closed_day7,
							'open_at' => $open_at,
							'close_at' => $open_at,
						]);
				
			CRUDBooster::redirect("/admin/Restaurant/detail/".$id,"Le restaurant ".$name." a été modifié avec succès !","success");
		}


	}