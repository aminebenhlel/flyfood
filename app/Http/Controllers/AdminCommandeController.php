<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;

	class AdminCommandeController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "commande";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Date","name"=>"created_at"];
			$this->col[] = ["label"=>"Client","name"=>"id_client","join"=>"client,full_name"];
			$this->col[] = ["label"=>"Livreur","name"=>"id_livreur","join"=>"livreur,name"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"Price","name"=>"prix", "callback"=>function($row) {
				return $row->prix.' DT';
			}];
			$this->col[] = ["label"=>"Adress","name"=>"adresse"];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          
			// $this->table_row_color[] = ['condition'=>"[status] == 'Ordred'","color"=>"success"];
			// $this->table_row_color[] = ['condition'=>"[status] == 'Canceled'","color"=>"danger"];
			// $this->table_row_color[] = ['condition'=>"[status] == 'In progress'","color"=>"warning"];
			// $this->table_row_color[] = ['condition'=>"[status] == 'Confirmed'","color"=>"info"];



	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        $this->style_css = "
				.status {
					width: 100px;
					height: 35px;
					font-weight: bold;
					padding: 7px 0px;
					text-align: center;
					border: 1px solid;
				}

				.confirmed {
					color: #337ab7;
					background-color: #d1eaff;
					border-color: #337ab7;
				}

				.inProgress {
					color: #777049;
					background-color: #fcf8e3;
					border-color: #777049;
				}

				.ordred {
					color: #3c763d;
					background-color: #dff0d8;
					border-color: #3c763d;
				}

				.canceled {
					color: #a94442;
					background-color: #f2dede;
					border-color: #a94442;
				}
			";
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	if($column_index == 3) {
				switch ($column_value) {
					case 'Confirmed':
						$column_value = '<div class="status confirmed">Confirmed</div>';
						break;

					case 'In progress':
						$column_value = '<div class="status inProgress">In progress</div>';
						break;

					case 'Canceled':
						$column_value = '<div class="status canceled">Canceled</div>';
						break;

					case 'Ordred':
						$column_value = '<div class="status ordred">Delivered</div>';
						break;
				}
			}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

		public function getDetail($id){
			if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$commande = DB::table('commande')
							->where('id', $id)
							->first();

			$client = DB::table('commande')
							->join('client', 'client.id', '=', 'commande.id_client')
							->select('client.full_name', 'client.phone')
							->where('client.id', $commande->id_client)
							->first();

			$commande->client_name = $client->full_name;
			$commande->client_phone = $client->phone;

			if($commande->id_livreur){
				$commande->id_livreur = DB::table('livreur')
										->where('id', $commande->id_livreur)
										->select('name')
										->first();
								
			}
			else{
				$commande->id_livreur = "Auncun Livreur assigné";
			}

			$products = (DB::table('commande_produit')
							->where('id_commande', $id)
							->get())->values()->all();

			$array= [];
			foreach ($products as $product) {
				$product->name = (DB::table('produit')
									->select('name')
									->where('id', $product->id_produit)
									->first())->name;

				$resto = DB::table('produit')
								->join('restaurant', 'restaurant.id', '=', 'produit.id_restaurant')
								->select('restaurant.name')
								->where('produit.id', $product->id_produit)
								->first();

				$commande_ingredients = (DB::table('commande_produit_ingredient')
								->join('ingredient', 'ingredient.id', '=', 'commande_produit_ingredient.id_ingredient')
								->select('ingredient.name')
								->where('commande_produit_ingredient.id_commande_produit', $product->id)
								->get())->values()->all();

				$product_ingredients = (DB::table('produit_ingredient')
								->join('ingredient', 'ingredient.id', '=', 'produit_ingredient.id_ingredient')
								->select('ingredient.name')
								->where('produit_ingredient.id_produit', $product->id_produit)
								->get())->values()->all();

				$exist = 0;
				$product->ingredients = [];
				foreach ($product_ingredients as $product_value) {
					$exist = 0;
					foreach ($commande_ingredients as $commande_value) {
						if($product_value->name == $commande_value->name){
							$exist = 1;
						}
					}
					if($exist == 0){
						array_push($product->ingredients,$product_value->name);
					}
				}

				$supplements = (DB::table('commande_produit_supplement')
								->join('supplement', 'supplement.id', '=', 'commande_produit_supplement.id_supplement')
								->select('supplement.name')
								->where('id_commande_produit', $product->id)
								->get())->values()->all();
				$product->supplements = $supplements;

				$options = (DB::table('commande_produit_options')
								->join('options', 'options.id', '=', 'commande_produit_options.id_options')
								->select('options.name')
								->where('id_commande_produit', $product->id)
								->get())->values()->all();
				$product->options = $options;
				
				if(!in_array($resto->name,$array)){
					array_push($array,$resto->name);
					$restaurants[$resto->name] = [];
				}
				array_push($restaurants[$resto->name], $product);
			}

			$livreurs = (DB::table('livreur')
				->select('id', 'name')
				->get())->values()->all();

			$data = [];
			$data['commande'] = $commande;
			$data['restaurants'] = $restaurants;
			$data['livreurs'] = $livreurs;
			// dd($data);
			return view('viewCommande',$data);
		}

		public function cancel_order(Request $request){
			DB::table('commande')
				->where('id', $request->id)
				->update(['status' => 'Canceled']);

			return redirect('admin/commande/detail/'.$request->id);
		}

		public function confirmeOrder(Request $request){
			// dd($request);
			DB::table('commande')
				->where('id', $request->id)
				->update([
						'status' => 'Confirmed',
						'id_livreur' => $request->livreur
						]);

			return redirect('admin/commande/detail/'.$request->id);
		}

		public function setOrdred(Request $request){
			DB::table('commande')
				->where('id', $request->id)
				->update(['status' => 'Ordred']);

			return redirect('admin/commande/detail/'.$request->id);
		}

	}