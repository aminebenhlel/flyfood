<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiProductsListingController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "produit";        
				$this->permalink   = "products_listing";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if($result['data']){
					foreach ($result['data'] as $line) {
						if($result['data']){
							if(DB::table('produit_favouris')->where('id_produit', $line->id)->where('id_client', $postdata['id_client'])->exists()){
								$line->is_favourite = 1;
							}
							else{
								$line->is_favourite = 0;
							}
						}
					}
				}
		    }

		}