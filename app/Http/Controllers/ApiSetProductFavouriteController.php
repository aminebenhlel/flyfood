<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiSetProductFavouriteController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "produit_favouris";        
				$this->permalink   = "set_product_favourite";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				if($postdata){
					if(DB::table('produit_favouris')->where('id_produit', $postdata['id_produit'])->where('id_client', $postdata['id_client'])->exists()){
						DB::table('produit_favouris')
							->where('id_produit', $postdata['id_produit'])
							->where('id_client', $postdata['id_client'])
							->delete();
						die();
					}
				}
		    }	

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
					return ;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				
		    }
		}