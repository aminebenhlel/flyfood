<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiRestaurantsListingController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "restaurant";        
				$this->permalink   = "restaurants_listing";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
				if($result['data']){
					foreach ($result['data'] as $line) {
						$categories = DB::table('restaurant_categories')
							->where('id_restaurant',$line->id)
							->join('produit_categorie', 'produit_categorie.id', '=', 'restaurant_categories.id_categories')
							->select('produit_categorie.id', 'produit_categorie.name')
							->get();

						if(DB::table('restaurant_favouris')->where('id_restaurant', $line->id)->where('id_client', $postdata['id_client'])->exists()){
							$line->is_favourite = 1;
						}
						else{
							$line->is_favourite = 0;
						}
						
						foreach ($categories as $i => $value) {
							$object[$value->id]= $value->name;
							//$object['name']= $value->name;
							//dd(json_encode($object));
							$line->categorie= $object;
						}
					}
				}
				//dd($result['data']);
		    }

		}