<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiUserIsexistController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "client";        
				$this->permalink   = "user_isexist";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if(DB::table('client')->where('email', $postdata['email'])->exists()){
					$result['api_message'] = "success";
				}
				else if(DB::table('client')->where('phone', $postdata['phone'])->exists()){
					$result['api_message'] = "success";
				}
		    }

		}