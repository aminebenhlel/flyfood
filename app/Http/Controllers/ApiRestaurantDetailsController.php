<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiRestaurantDetailsController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "restaurant";        
				$this->permalink   = "restaurant_details";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				if($result){
					if(DB::table('restaurant_favouris')->where('id_restaurant', $postdata['id'])->where('id_client', $postdata['id_client'])->exists()){
						$result['is_favourite'] = 1;
					}
					else{
						$result['is_favourite'] = 0;
					}
				}
		    }

		}