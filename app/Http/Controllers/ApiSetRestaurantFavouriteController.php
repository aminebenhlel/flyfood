<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiSetRestaurantFavouriteController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "restaurant_favouris";        
				$this->permalink   = "set_restaurant_favourite";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				if($postdata){
					if(DB::table('restaurant_favouris')->where('id_restaurant', $postdata['id_restaurant'])->where('id_client', $postdata['id_client'])->exists()){
						DB::table('restaurant_favouris')
							->where('id_restaurant', $postdata['id_restaurant'])
							->where('id_client', $postdata['id_client'])
							->delete();
						die();
					}
				}
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}