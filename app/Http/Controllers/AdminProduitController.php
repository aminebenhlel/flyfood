<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use File;
	class AdminProduitController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "produit";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Product Categorie","name"=>"id_produit_categorie","join"=>"produit_categorie,name"];
			$this->col[] = ["label"=>"Restaurant","name"=>"id_restaurant","join"=>"restaurant,name"];
			$this->col[] = ["label"=>"Name","name"=>"name"];
			$this->col[] = ["label"=>"Price","name"=>"prix", "callback"=>function($row) {
				return $row->prix.' DT';
			}];
			$this->col[] = ["label"=>"photo","name"=>"Photo","image"=>true];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Produit Categorie','name'=>'id_produit_categorie','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'produit_categorie,name'];
			$this->form[] = ['label'=>'Restaurant','name'=>'id_restaurant','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'restaurant,name'];
			$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Prix','name'=>'prix','type'=>'number','validation'=>'required|min:0||numeric','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Max Option','name'=>'max_option','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Min Option','name'=>'min_option','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Max Supplement','name'=>'max_supplement','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Value','name'=>'value','type'=>'text','validation'=>'required|string|min:3|max:50','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Produit Categorie','name'=>'id_produit_categorie','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'produit_categorie,name'];
			//$this->form[] = ['label'=>'Restaurant','name'=>'id_restaurant','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'restaurant,name'];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Prix','name'=>'prix','type'=>'number','validation'=>'required|min:0||numeric','width'=>'col-sm-10',"step"=> "0.1"];
			//$this->form[] = ['label'=>'Max Option','name'=>'max_option','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Min Option','name'=>'min_option','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Max Supplement','name'=>'max_supplement','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Value','name'=>'value','type'=>'text','validation'=>'required|string|min:3|max:50','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {

			$photo = (DB::table('produit')
						->where('id', $id)
						->select('photo')
						->first())->photo;

			

			if (File::exists(public_path($photo))) {
				File::delete(public_path($photo));
			}

			DB::table('produit_supplement')
					->where('id_produit', $id)
					->delete();

			DB::table('produit_ingredient')
					->where('id_produit', $id)
					->delete();

			DB::table('produit_options_groupe')
					->where('id_produit', $id)
					->delete();

			DB::table('produit_options')
					->where('id_produit', $id)
					->delete();

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

		public function getAdd() {
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$restaurants = DB::table('restaurant')
				->select('id','name')
				->orderBy('name', 'ASC')
				->get();

			$categories1 = DB::table('produit_categorie')
				->select('id','name')
				->where('name','Autre')
				->first();
			
			$categories2 = DB::table('produit_categorie')
				->select('id','name')
				->where('name','not like','Autre')
				->orderBy('name', 'ASC')
				->get();
			
			$i = 0;
			if($categories1) {
				$categories[$i++] = $categories1;
			}
			foreach($categories2 as $categorie){
				$categories[$i]=$categorie;
				$i++;
			}

			$supplements = DB::table('supplement')
				->select('id','name','prix')
				->orderBy('name', 'ASC')
				->get();

			$ingredients = DB::table('ingredient')
				->select('id','name')
				->orderBy('name', 'ASC')
				->get();

			$group_options = DB::table('options_groupe')
				->select('id','name')
				->orderBy('name', 'ASC')
				->get();

			$data = [];
			$data['page_title'] = 'Add Product';
			$data['categories'] = $categories;
			$data['restaurants'] = $restaurants;
			$data['supplements'] = $supplements;
			$data['ingredients'] = $ingredients;
			
			return view('addProduct',$data);
		}

		public function getRestoData(Request $request){
			$id_restaurant = $request->id_restaurant;

			$supplements = DB::table('supplement')
				->select('id','name','prix')
				->where('id_restaurant',$id_restaurant)
				->orderBy('name', 'ASC')
				->get();
			
			$ingredients = DB::table('ingredient')
				->select('id','name')
				->where('id_restaurant',$id_restaurant)
				->orderBy('name', 'ASC')
				->get();
			
			$options_groupe = DB::table('options_groupe')
				->select('id','name','min','max')
				->where('id_restaurant',$id_restaurant)
				->orderBy('name', 'ASC')
				->get();

			foreach ($options_groupe as $groupe) {
				$groupe->options = DB::table('options')
									->select('id','name','prix')
									->where('id_options_groupe', $groupe->id )
									->orderBy('prix', 'ASC')
									->get();
			}

			return [
				'supplements' => $supplements,
				'ingredients' => $ingredients,
				'options_groupe' => $options_groupe
			];
		}

		public function addSupplement(Request $request){
			$id_restaurant = $request->id_restaurant;
			$name = $request->name;
			$prix = $request->prix;

			if((DB::table('supplement')->where('name',$name)->where('prix',$prix)->exists())){
				$exist = 1;
			}
			else{
				$exist = 0;
				$id = DB::table('supplement')
					->insertGetId([
						'id_restaurant' => $id_restaurant,
						'name' => $name,
						'prix' => $prix
				]);
			}
			

			return [
				'exist' => $exist,
				'id' => $id,
				'name' => $name,
				'prix' => $prix
			];
		}

		public function updatesupplement(Request $request){
			$id_restaurant = $request->id_restaurant;
			$id = $request->id;
			$newname = $request->newname;
			$prix = $request->prix;

			if((DB::table('supplement')->where('name',$newname)->where('prix',$prix)->exists())){
				$exist = 1;
			}
			else{
				$exist = 0;
				DB::table('supplement')
					->where('id',$id)
					->update([
								'name' => $newname,
								'prix' => $prix
							]);
			}

			return [
				'exist' => $exist,
				'name' => $newname,
				'prix' => $prix
			];
		}

		public function delsupplement(Request $request){
			$id = $request->id;
			if((DB::table('supplement')->where('id',$id)->exists())){
				DB::table('supplement')
					->where('id',$id)
					->delete();

				if((DB::table('produit_supplement')->where('id',$id)->exists())){
						DB::table('produit_supplement')
							->where('id_supplement',$id)
							->delete();
					}
			}
			
		}

		public function addingredient(Request $request){
			$id_restaurant = $request->id_restaurant;
			$name = $request->name;
			
			if((DB::table('ingredient')->where('name',$name)->exists())){
				$exist = 1;
			}
			else{
				$exist = 0;
				$id = DB::table('ingredient')
				->insertGetId([
					'id_restaurant' => $id_restaurant,
					'name' => $name,
				]);
			}

			return [
				'id' => $id,
				'name' => $name,
				'exist' => $exist
			];
		}

		public function updateingredient(Request $request){
			$id_restaurant = $request->id_restaurant;
			$id = $request->id;
			$newname = $request->newname;

			if((DB::table('ingredient')->where('name',$newname)->exists())){
				$exist = 1;
			}
			else{
				$exist = 0;
				DB::table('ingredient')
					->where('id',$id)
					->update([
								'name' => $newname,
							]);
			}

			return [
				'exist' => $exist,
				'name' => $newname,
				'prix' => $prix
			];
		}

		public function delingredient(Request $request){
			$id = $request->id;
			if((DB::table('ingredient')->where('id',$id)->exists())){
				DB::table('ingredient')
					->where('id',$id)
					->delete();
				
				if((DB::table('produit_ingredient')->where('id',$id)->exists())){
						DB::table('produit_ingredient')
							->where('id_ingredient',$id)
							->delete();
					}
			}

		}
		
		public function addOptions(Request $request){ 
			$id_restaurant = $request->id_restaurant;
			$groupe_name = $request->groupe_name;
			$min = $request->min;
			$max = $request->max;
			$options = $request->options;
			$prix = $request->prix;
			$options_id = [];

			if((DB::table('options_groupe')->where('name',$groupe_name)->where('id_restaurant', $id_restaurant)->exists())){
				$exist = 1;
			}
			else{
				$exist = 0;
				$groupe_id = DB::table('options_groupe')
					->insertGetId([
						'id_restaurant' => $id_restaurant,
						'name' => $groupe_name,
						'min' => $min,
						'max' => $max,
					]);
			
				for($i=0;$i<count($options);$i++){
					$option_id = DB::table('options')
					->insertGetId([
						'id_options_groupe' => $groupe_id,
						'name' => $options[$i],
						'prix' => $prix[$i],
					]);
					$options_id[$i] = $option_id;
			}
			}

			return [
				'exist' => $exist,
				'groupe_id' => $groupe_id,
				'groupe_name' => $groupe_name,
				'min' => $min,
				'max' => $max,
				'options_id' => $options_id,
				'options_name' => $options,
				'options_prix' => $prix
			];
		}

		public function updateOptions(Request $request){
			$id = $request->idGroupeUpdate;
			$OldGroupeName = $request->oldGroupeNameUpdate;
			$NewGroupeName = $request->newGroupeNameUpdate;
			$min = $request->newminUpdate;
			$max = $request->newmaxUpdate;
			$optionsName = $request->newoptionsUpdate;
			$optionsPrix = $request->prixUpdate;
			// $optionsPrix = array_map(function($value) {
			// 	return intval($value);
			// }, $request->prixUpdate);

			if(($OldGroupeName!=$NewGroupeName)&&(DB::table('options_groupe')->where('name',$NewGroupeName)->exists())){
				$exist = 1;
			}
			else{
				$exist = 0;
				DB::table('options_groupe')
					->where('id' , $id)
					->update([
						'name' => $NewGroupeName,
						'min' => $min,
						'max' => $max
					]);
				
				
				$t[]= DB::table('options')
						->select('id')
						->where('id_options_groupe' , $id)
						->get();
				$n= count($t[0]);
				
				if($n==count($optionsName)){
					for($i=0;$i<$n;$i++){
						DB::table('options')
							->where('id' , $t[0][$i]->id)
							->update(['name' => $optionsName[$i],
									  'prix' => $optionsPrix[$i]	
									 ]);
					}
				}
				elseif($n<count($optionsName)){
					for($i=0;$i<$n;$i++){
						DB::table('options')
							->where('id' , $t[0][$i]->id)
							->update([
								'name' => $optionsName[$i],
								'prix' => $optionsPrix[$i]
							]);
					}
					for($i=$n;$i<count($optionsName);$i++){
						DB::table('options')
							->insert([
								'id_options_groupe' => $id,
								'name' => $optionsName[$i],
								'prix' => $optionsPrix[$i],
							]);
					}
				}
				elseif($n>count($optionsName)){
					for($i=0;$i<count($optionsName);$i++){
						DB::table('options')
							->where('id' , $t[0][$i]->id)
							->update([
								'name' => $optionsName[$i],
								'prix' => $optionsPrix[$i]	
							]);
					}
					for($i=count($optionsName);$i<$n;$i++){
						DB::table('options')
							->where('id' , $t[0][$i]->id)
							->delete();
					}
				}
			}
			
			return[
					'exist' => $exist
			];
		}

		public function delOptions(Request $request){
			$id = $request->id;
			if((DB::table('options_groupe')->where('id',$id)->exists())){
				DB::table('options_groupe')
					->where('id',$id)
					->delete();
				
				if((DB::table('produit_options_groupe')->where('id',$id)->exists())){
						DB::table('produit_options_groupe')
							->where('id_options_groupe',$id)
							->delete();
				}

				if((DB::table('options')->where('id',$id)->exists())){
					DB::table('produit_options')
							->where('options.id_options_groupe',$id)
							->join('options', 'options.id', '=', 'produit_options.id_options')
							->delete();
							
					DB::table('options')
						->where('id_options_groupe',$id)
						->delete();
				}
			}

		}

		public function addProduct(Request $request){
			$id_restaurant = $request->id_restaurants;
			$id_categorie = $request->id_categories;
			$name = $request->name;
			$prix = $request->prix;
			$photo = $request->photo;
			$max_supplement = $request->max_supplement;
			$supplements_id = json_decode($request->supplementJson);
			$ingredients_id = json_decode($request->ingredientJson);
			$options_groupe = $request->options_groupe;
			$min = $request->min;
			$max = $request->max;
			$options = $request->options;
			$description = $request->description;


			if($photo){
				$photo->move(public_path('/uploads'), time().'_'.$request->file('photo')->getClientOriginalName());
				$photo= '/uploads/'.time().'_'.$request->file('photo')->getClientOriginalName();
			}

			$id_produit = DB::table('produit')
				->insertGetId([
					'id_produit_categorie' => $id_categorie,
					'id_restaurant' => $id_restaurant,
					'name' => $name,
					'prix' => $prix,
					'max_supplement' => $max_supplement,
					'description' => $description,
					'photo' => $photo
				]);
			
			
			if ($supplements_id) {
				foreach ($supplements_id as $supplement) {
					DB::table('produit_supplement')
					->insert([
						'id_produit' => $id_produit,
						'id_supplement' => $supplement->id,
					]);
				}
			}

			if ($ingredients_id) {
				foreach ($ingredients_id as $ingredient) {
					DB::table('produit_ingredient')
					->insert([
						'id_produit' => $id_produit,
						'id_ingredient' => $ingredient->id,
					]);
				}
			}
			
			if($options_groupe){
				for($i=0;$i<count($options_groupe);$i++){
					DB::table('produit_options_groupe')
						->insert([
							'id_produit' => $id_produit,
							'id_options_groupe' => $options_groupe[$i],
							'min' => $min[$i],
							'max' => $max[$i]
						]);
				}
			}
			if($options){
				foreach($options as $option){
					DB::table('produit_options')
						->insert([
							'id_produit' => $id_produit,
							'id_options' => $option,
						]);
				}
			}

			CRUDBooster::redirect("/admin/produit/detail/".$id_produit,"Le produit ".$name." a été ajouté avec succès !","success");
			
		}

		public function getEdit($id) {
			//Create an Auth
			if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$produit = DB::table('produit')
				->where('id' , $id)
				->first();

			$name_restaurant = DB::table('restaurant')
				->select('name')
				->where('id',$produit->id_restaurant)
				->first();

			$name_categorie = DB::table('produit_categorie')
				->select('name')
				->where('id',$produit->id_produit_categorie)
				->first();

			$selected_supplements = DB::table('produit_supplement')
				->select('id_supplement')
				->where('id_produit',$id)
				->get();
			
			foreach ($selected_supplements as $i => $value) {
				$array[$i]=$value->id_supplement;
			}
			$selected_supplements = $array;

			$selected_ingredients = DB::table('produit_ingredient')
				->select('id_ingredient')
				->where('id_produit',$id)
				->get();
			
			$array=null;
			foreach ($selected_ingredients as $i => $value) {
				$array[$i]=$value->id_ingredient;
			}
			$selected_ingredients = $array;

			$categories1 = DB::table('produit_categorie')
				->select('id','name')
				->where('name','Autre')
				->first();
			
			$categories2 = DB::table('produit_categorie')
				->select('id','name')
				->where('name','not like','Autre')
				->orderBy('name', 'ASC')
				->get();
			
			$categories[0] = $categories1;
			$i=1;
			foreach($categories2 as $categorie){
				$categories[$i]=$categorie;
				$i++;
			}
			
			$supplements = DB::table('supplement')
				->select('id','name','prix')
				->where('id_restaurant',$produit->id_restaurant)
				->orderBy('name', 'ASC')
				->get();

			$ingredients = DB::table('ingredient')
				->select('id','name')
				->where('id_restaurant',$produit->id_restaurant)
				->orderBy('name', 'ASC')
				->get();

			$options_groupe = DB::table('options_groupe')
				->select('id','name','min','max')
				->where('id_restaurant',$produit->id_restaurant)
				->orderBy('name', 'ASC')
				->get();

			foreach ($options_groupe as $groupe) {
				$groupe->is_selected = 0;
				if (DB::table('produit_options_groupe')->where('id_produit',$id)->where('id_options_groupe', $groupe->id)->exists()) {
					$groupe->is_selected = 1;

					$groupe->min = ( DB::table('produit_options_groupe')
										->where('id_options_groupe',$groupe->id)
										->select('min')
										->first())->min;

					$groupe->max = ( DB::table('produit_options_groupe')
										->where('id_options_groupe',$groupe->id)
										->select('max')
										->first())->max;
				}
				$groupe->options = DB::table('options')
									->select('id','name','prix')
									->where('id_options_groupe', $groupe->id )
									->orderBy('prix', 'ASC')
									->get();
				foreach ($groupe->options as $option) {
					$option->is_selected = 0;
					if (DB::table('produit_options')->where('id_produit',$id)->where('id_options', $option->id)->exists()) {
						$option->is_selected = 1;
					}
				}
			}
			
			$data = [];
			$data['page_title'] = 'Restaurant : '.$name_restaurant->name;
			$data['restaurant_id'] = $produit->id_restaurant;
			$data['categories'] = $categories;
			$data['supplements'] = $supplements;
			$data['ingredients'] = $ingredients;
			$data['name_categorie'] = $name_categorie;
			$data['produit'] = $produit;
			$data['selected_supplements'] = $selected_supplements;
			$data['selected_ingredients'] = $selected_ingredients;
			$data['selected_options'] = $options_groupe;

			return view('editProduct',$data);
		}

		public function editProduct(Request $request){
			$id_produit = $request->id_produit;
			$id_categorie = $request->id_categories;
			$name = $request->name;
			$prix = $request->prix;
			$photo= $request->photo;
			$max_supplement = $request->max_supplement;
			$supplements_id = json_decode($request->supplementJson);
			$ingredients_id = json_decode($request->ingredientJson);
			$options_groupe = $request->options_groupe;
			$min = $request->min;
			$max = $request->max;
			$options = $request->options;
			$description = $request->description;
			
			DB::table('produit')
				->where('id', $id_produit)
				->update([
					'id_produit_categorie' => $id_categorie,
					'name' => $name,
					'prix' => $prix,
					'max_supplement' => $max_supplement,
					'description' => $description
				]);

			if($photo){
				$photo->move(public_path('/uploads'), time().'_'.$request->file('photo')->getClientOriginalName());
				$photo= '/uploads/'.time().'_'.$request->file('photo')->getClientOriginalName();
				DB::table('produit')
					->where('id', $id_produit)
					->update(['photo' => $photo]);
			}

			DB::table('produit_supplement')
				->where('id_produit', $id_produit)
				->delete();
			
			if ($supplements_id) {
				foreach ($supplements_id as $supplement) {
					DB::table('produit_supplement')
					->insert([
						'id_produit' => $id_produit,
						'id_supplement' => $supplement->id,
					]);
				}
			}

			DB::table('produit_ingredient')
				->where('id_produit', $id_produit)
				->delete();

			if ($ingredients_id) {
				foreach ($ingredients_id as $ingredient) {
					DB::table('produit_ingredient')
					->insert([
						'id_produit' => $id_produit,
						'id_ingredient' => $ingredient->id,
					]);
				}
			}

			DB::table('produit_options_groupe')
				->where('id_produit', $id_produit)
				->delete();

			if($options_groupe){
				for($i=0;$i<count($options_groupe);$i++){
					DB::table('produit_options_groupe')
						->insert([
							'id_produit' => $id_produit,
							'id_options_groupe' => $options_groupe[$i],
							'min' => $min[$i],
							'max' => $max[$i]
						]);
				}
			}

			DB::table('produit_options')
				->where('id_produit', $id_produit)
				->delete();

			if ($options) {
				foreach($options as $option_id){
					DB::table('produit_options')
						->insert([
							'id_produit' => $id_produit,
							'id_options' => $option_id,
						]);
				}
			}
				
			CRUDBooster::redirect("/admin/produit/detail/".$id_produit,"The status product has been Updated !","info");
		}

		// public function setdelete($id) {
		// 	if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
		// 		CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		// 	}

		// 	DB::table('produit')
		// 			->where('id', $id)
		// 			->delete();

		// 	DB::table('produit_supplement')
		// 			->where('id_produit', $id)
		// 			->delete();

		// 	DB::table('produit_ingredient')
		// 			->where('id_produit', $id)
		// 			->delete();

		// 	CRUDBooster::redirect("/admin/produit","The status product has been deleted !","info");
		// }

		public function getDetail($id) {
			if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$produit = DB::table('produit')
				->where('id' , $id)
				->first();

			$name_restaurant = DB::table('restaurant')
				->select('name')
				->where('id',$produit->id_restaurant)
				->first();

			$name_categorie = DB::table('produit_categorie')
				->select('name')
				->where('id',$produit->id_produit_categorie)
				->first();

			$selected_supplements = DB::table('supplement')
				->join('produit_supplement', 'produit_supplement.id_supplement', '=', 'supplement.id')
				->where('produit_supplement.id_produit',$id)
				->select('supplement.name','supplement.prix')
				->get();

			$selected_ingredients = DB::table('ingredient')
				->join('produit_ingredient', 'produit_ingredient.id_ingredient', '=', 'ingredient.id')
				->where('produit_ingredient.id_produit',$id)
				->select('ingredient.name')
				->get();

			$options_groupe = DB::table('produit_options_groupe')
				->join('options_groupe', 'options_groupe.id', '=', 'produit_options_groupe.id_options_groupe')
				->select('options_groupe.id','options_groupe.name','produit_options_groupe.min','produit_options_groupe.max')
				->where('id_produit',$id)
				->orderBy('options_groupe.name', 'ASC')
				->get();

			foreach ($options_groupe as $groupe) {
				$groupe->options = DB::table('options')
									->join('produit_options', 'produit_options.id_options', '=', 'options.id')
									->select('options.name','options.prix')
									->where('produit_options.id_produit',$id)
									->where('options.id_options_groupe', $groupe->id )
									->orderBy('options.prix', 'ASC')
									->get();
			}
			
			$data = [];
			$data['page_title'] = 'Restaurant : '.$name_restaurant->name;
			$data['name_categorie'] = $name_categorie;
			$data['produit'] = $produit;
			$data['selected_supplements'] = $selected_supplements;
			$data['selected_ingredients'] = $selected_ingredients;
			$data['selected_options'] = $options_groupe;
			return view('viewProduct',$data);
		}
}