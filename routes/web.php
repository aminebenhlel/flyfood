<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/admin/produit/addsupplement','AdminProduitController@addsupplement')->name('addsupplement');

Route::post('/admin/produit/updatesupplement','AdminProduitController@updatesupplement')->name('updatesupplement');

Route::post('/admin/produit/delsupplement','AdminProduitController@delsupplement')->name('delsupplement');

Route::post('/admin/produit/addingredient','AdminProduitController@addingredient')->name('addingredient');

Route::post('/admin/produit/updateingredient','AdminProduitController@updateingredient')->name('updateingredient');

Route::post('/admin/produit/delingredient','AdminProduitController@delingredient')->name('delingredient');

Route::post('/admin/produit/addoptions','AdminProduitController@addOptions')->name('addoptions');

Route::post('/admin/produit/updateOptions','AdminProduitController@updateOptions')->name('updateOptions');

Route::post('/admin/produit/delOptions','AdminProduitController@delOptions')->name('delOptions');

Route::post('/admin/produit/addproduct','AdminProduitController@addProduct')->name('addproduct');

Route::post('/admin/produit/editproduct','AdminProduitController@editProduct')->name('editproduct');

Route::get('/admin/produit/setdelete/{id}','AdminProduitController@setdelete')->name('setdelete');

Route::post('/admin/restaurant/getRestoData','AdminProduitController@getRestoData')->name('getRestoData');

Route::post('/admin/restaurant/editRestaurant','AdminRestaurant27Controller@editRestaurant')->name('editRestaurant');

Route::post('/admin/restaurant/addRestaurant','AdminRestaurant27Controller@addRestaurant')->name('addRestaurant');

Route::post('/admin/commande/detail/confirm_order/{id}','AdminCommandeController@confirmeOrder')->name('confirm_order');

Route::post('/admin/commande/detail/cancel_order/{id}','AdminCommandeController@cancel_order')->name('cancel_order');

Route::post('/admin/commande/detail/set_order_ordred/{id}','AdminCommandeController@setOrdred')->name('set_order_ordred');