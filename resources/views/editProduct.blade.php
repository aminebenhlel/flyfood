<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
<link rel="stylesheet" href="{{asset('/css/nestable.css')}}">
<link rel="stylesheet" href="{{asset('/css/jquery.steps.css')}}">
<style>
  .wizard > .content{
    min-height: 39em;
  }
  .dd{
    width : 100%;
  }
  .dd-list{
    height: fit-content;
    height: 250px;
    overflow: auto;
  }
  li{
    cursor: move;
  }
  .options{
    display: flex;
  }
  .option-container{
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: #ffd491;
    padding: 11px 11px;
    border-radius:  5px 5px 0 0 ;
    color: #fff;
    cursor: pointer;
    margin: 3px 0;
  }
  .checkbox-label{
    display: flex;
    align-items: center;
  }
  .checkbox-label label{
    margin-left: 8px;
    margin-bottom: 0;
  }
  .checkbox-label input{
    width: 20px;
    height: 20px;
    margin-top: 0;
  }
  .option-group {
    padding: 20px;
    border: solid 1px #ccc;
    border-top: none;
    border-radius: 0 0 5px 5px;
  }
  .custom-checkbox{
    margin-right: 10px !important;
  }
  #options{
    max-height: 100%;
    overflow: auto;
  }
  .close-btn{
    background-color: #0000002e!important;
  }
  .buttooons{
    /* display: flex;
    justify-content: space-between; */
  }
  .btns{
    width : 150px;
  }
  .description_group{
    display: flex;
  }
  .description_div{
    width: 100%;
    margin-left: 33px;
  }
  .description{
    
  }

  .dp-flex {
      display: flex;
      width: 100%;
      justify-content: space-evenly;
      flex-wrap: wrap;
  }

  #addAllSupplements, #addNewSupplement, #addAllIngredients, #addNewIngredient {
    min-width: 190px;
    margin: 3px 0;
  }

  #addAllSupplements span, #addNewSupplement span, #addAllIngredients span, #addNewIngredient span {
    float: left;
    padding-top: 3px;
  }

  .wizard > .steps > ul > li {
    width: 20%!important;
  }

  .wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active {
    background: #ffb743;
  }

  .wizard > .steps .done a, .wizard > .steps .done a:hover, .wizard > .steps .done a:active {
    background: #ffd9ab;
  }

  .wizard a, .tabcontrol a {
    height: 75px !important;
    display: flex !important;
    align-items: center;
    justify-content: center;
    margin-top: 10px !important;
}

  .wizard > .steps .number {
    float: left;
    margin-right: 10px;
}

.wizard > .actions a {
    height: 37px !important;
    width: 90px !important;
}

.wizard > .content > .body label.error {
    margin-bottom: 0 !important;
}

textarea {
  resize: vertical;
  min-height: 100px;
}

.option-group div {
    margin: 5px 0;
}

#addoption, .remove_option {
    width: 26px;
    height: 26px;
}
</style>
@section('content')
<p><a title="Return" href="{{ URL::asset('index.php/admin/produit')}}"><i class="fa fa-chevron-circle-left "></i>&nbsp; Back to prudcts list</a></p>
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class="panel-heading">
      <strong><i class="fa fa-spoon"></i> Edit product : {{$produit->name}}</strong>
    </div>

      <form class="form-horizontal" id="formAddProduct" method='post' enctype="multipart/form-data" action='{{route('editproduct')}}'>
        @csrf

        <div id="stepsDiv">
          <h2>General informations</h2>
          <section>

          <div class='form-group'>
            <input type="hidden" name="id_produit" value="{{$produit->id}}">
            <label class="control-label col-sm-2">Categories
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
              <select style="width:100%" class="form-control select2-hidden-accessible" id="id_categories" required="" name="id_categories" tabindex="-1" aria-hidden="true">
                  @foreach($categories as $categorie)
                    @if ($categorie->name==$name_categorie->name)
                        <option value="{{ $categorie->id }}" selected="selected">{{ $categorie->name }}</option>
                    @else
                        <option value="{{ $categorie->id }}" >{{ $categorie->name }}</option>
                    @endif
                  @endforeach
              </select>
            </div>
          </div>

          <div class='form-group'>
            <label class="control-label col-sm-2">Product name
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
              <input type="text" title="Name" required="" placeholder="You can only enter the letter only" maxlength="70" class="form-control" name="name" id="name" value="{{$produit->name}}">
              <div class="text-danger"></div>
              <p class="help-block"></p>
            </div>
          </div>

          <div class='form-group'>
            <label class="control-label col-sm-2">Price
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
              <input type="number" title="Prix" required="" class="form-control" name="prix" id="prix" value="{{$produit->prix}}" step="any">
              <div class="text-danger"></div>
              <p class="help-block"></p>
            </div>
          </div>

          <div class='form-group'>
            <label class="control-label col-sm-2">Max supplement
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
              <input type="number" title="max_supplement" required="" class="form-control" name="max_supplement" id="max_supplement" value="{{$produit->max_supplement}}" step="1">
              <div class="text-danger"></div>
              <p class="help-block"></p>
            </div>
          </div>

          <div class='form-group'>
            <label class="control-label col-sm-2">Photo
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
                <a data-lightbox="roadtrip" id="a_photo" href="{{ URL::asset('')}}{{$produit->photo}}"><img style="max-width:150px" id="img_photo" title="Image For Photo" src="{{ URL::asset('')}}{{$produit->photo}}"></a>
                <div style="margin-top: 10px;">
                    <input type="file" id="photo" title="Photo" class="form-control" name="photo" accept="image/*">
                    <p class="help-block">File types support : JPG, JPEG, PNG, GIF, BMP</p>
                    <div class="text-danger"></div>
                </div>
              </div>
          </div>

          </section>

          <h2>Description</h2>
          <section>
            <div class='description_group'>
              <label class="control-label col-sm-2">Description
              </label>
              <div class="description_div">
                <textarea name="description" class="description form-control" rows="20" cols="50" maxlength="255">{{$produit->description}}</textarea>
              </div>
            </div>
          </section>

          <h2>Supplements</h2>
          <section>
          <div class='form-group'>
            <div class="text-center">
              <h4 style="margin-bottom: 25px;">Drag and drop supplements to add them</h4>
            </div>
            <input type="hidden" id="supplementJson" value="" name="supplementJson">
            <div>
              <div class="row">
                <div class="col-sm-6">
                <label>Supplements <span class="text-danger" title="This field is required">*</span></label>
                @if ($selected_supplements==null)
                  <div class="dd" id="nest1">
                      <div class="dd-empty" style="background-color: #f5f5f56c">
                      </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <label>Drag from the supplements below</label>
                  <div class="dd" id="nest2">
                      <ol class="dd-list">
                          @foreach($supplements as $supplement)
                              <li class="dd-item" data-id="{{$supplement->id}}">
                                  <div class="dd-handle unselectable">{{$supplement->name}} | Prix : {{$supplement->prix}} Dt</div>
                              </li>
                          @endforeach
                      </ol>
                @elseif(count($selected_supplements)==count($supplements))
                  <div class="dd" id="nest1">
                    <ol class="dd-list">
                        @foreach($supplements as $supplement)
                            <li class="dd-item" data-id="{{$supplement->id}}">
                                <div class="dd-handle unselectable">{{$supplement->name}} | Prix : {{$supplement->prix}}</div>
                            </li>
                        @endforeach
                    </ol>
                  </div>
                </div>
                <div class="col-sm-6">
                  <label>Drag from the supplements below</label>
                  <div class="dd" id="nest2">
                      <div class="dd-empty" style="background-color: #f5f5f56c">
                      </div>
                @else
                  <div class="dd" id="nest1" >
                      <ol class="dd-list">
                          @foreach($supplements as $supplement)
                              @if (in_array($supplement->id,$selected_supplements))
                                  <li class="dd-item" data-id="{{$supplement->id}}">
                                      <div class="dd-handle unselectable">{{$supplement->name}} | Prix : {{$supplement->prix}}</div>
                                  </li>
                              @endif
                          @endforeach
                      </ol>
                  </div>
                </div>
                <div class="col-sm-6">
                  <label>Drag from the supplements below</label>
                  <div class="dd" id="nest2">
                      <ol class="dd-list">
                          @foreach($supplements as $supplement)
                              @if (!in_array($supplement->id,$selected_supplements))
                                  <li class="dd-item" data-id="{{$supplement->id}}">
                                      <div class="dd-handle unselectable">{{$supplement->name}} | Prix : {{$supplement->prix}}</div>
                                  </li>
                              @endif
                          @endforeach
                      </ol>
                @endif
                  </div>
                  <div class="dp-flex">
                    <button type="button" class="btn btn-primary btns" id="addAllSupplements"><span class="fa fa-reply-all"></span> Add all supplements</button>
                    <button id="addNewSupplement" type="button" class="btn btn-success btns" data-toggle="modal" data-target="#ModalAddNewSupplement"><span class="fa fa-plus"></span> Add New Supplement</button>
                  </div>
                </div>
              </div>
            <div class="text-danger"></div>
              <p class="help-block"></p>
          </div>
          </div>
          </section>

          <h2>Optional ingredients</h2>
          <section>
            <div class='form-group'>
              <div class="text-center">
                <h4 style="margin-bottom: 25px;">Drag and drop ingredients to add them</h4>
              </div>
              <input type="hidden" id="ingredientJson" value="" name="ingredientJson">
              <div>
                <div class="row">
                  <div class="col-sm-6">
                    <label>Optional ingredients <span class="text-danger" title="This field is required">*</span> <i title="Optional ingredients are ingredients that can be removed when ordering" data-toggle="tooltip" class="fa fa-question-circle"></i></label>
                    @if ($selected_ingredients==null)
                        <div class="dd" id="nest3">
                            <div class="dd-empty" style="background-color: #f5f5f56c">
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label>Drag from the ingredients below</label>
                        <div class="dd" id="nest4">
                            <ol class="dd-list">
                              @foreach($ingredients as $composant)
                              <li class="dd-item" data-id="{{$composant->id}}">
                                  <div class="dd-handle unselectable">{{$composant->name}}</div>
                              </li>
                              @endforeach
                            </ol>
                    @elseif(count($selected_ingredients)==count($ingredients))
                        <div class="dd" id="nest3">
                            <ol class="dd-list">
                                @foreach($ingredients as $composant)
                                    <li class="dd-item" data-id="{{$composant->id}}">
                                        <div class="dd-handle unselectable">{{$composant->name}}</div>
                                    </li>
                              @endforeach
                            </ol>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label>Drag from the ingredients below</label>
                        <div class="dd" id="nest4">
                            <div class="dd-empty" style="background-color: #f5f5f56c">
                            </div>
                    @else
                        <div class="dd" id="nest3">
                            <ol class="dd-list">
                                @foreach($ingredients as $composant)
                                    @if (in_array($composant->id,$selected_ingredients))
                                        <li class="dd-item" data-id="{{$composant->id}}">
                                            <div class="dd-handle unselectable">{{$composant->name}}</div>
                                        </li>
                                    @endif
                                @endforeach
                            </ol>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label>Drag from the ingredients below</label>
                        <div class="dd" id="nest4">
                            <ol class="dd-list">
                                @foreach($ingredients as $composant)
                                    @if (!in_array($composant->id,$selected_ingredients))
                                        <li class="dd-item" data-id="{{$composant->id}}">
                                            <div class="dd-handle unselectable">{{$composant->name}}</div>
                                        </li>
                                    @endif
                                @endforeach
                            </ol>
                    @endif
                  </div>
                  <div class="dp-flex">
                    <button type="button" class="btn btn-primary btns" id="addAllIngredients"><span class="fa fa-reply-all"></span> Add all ingredients</button>
                    <button type="button" class="btn btn-success btns" id="addNewIngredient" data-toggle="modal" data-target="#ModalAddNewIngredient"><span class="fa fa-plus"></span> Add New Ingredient</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-danger"></div>
            <p class="help-block"></p>
            </div>
          </section>

          <h2>Options</h2>
          <section>
            <div class='form-group'>
              <label class="control-label col-sm-2">Options
                <span class="text-danger" title="This field is required">*</span>
              </label>
              <div class="col-sm-5">
                <div id="options">
                  @foreach ($selected_options as $groupe)
                      @php
                        $content = '';
                        $content = $content. '<div class="option">';
                        $content = $content. '<div class="option-container" id="group'.$groupe->id.'">';
                        $content = $content. '<div class="checkbox-label">';
                        if($groupe->is_selected==1){
                          $content = $content. '<input  class="checkbox" checked type="checkbox" value="'.$groupe->id.'" id="'.$groupe->id.'" name="options_groupe[]"><label>'.$groupe->name.'</label>';
                        }
                        else{
                          $content = $content. '<input  class="checkbox" type="checkbox" value="'.$groupe->id.'" id="'.$groupe->id.'" name="options_groupe[]"><label>'.$groupe->name.'</label>';
                        }
                        $content = $content. '</div>';
                        $content = $content. '<div class="dropdown">';
                        $content = $content. '<i class="fa fa-plus"></i>';
                        $content = $content. '</div>';
                        $content = $content. '</div>';
                        $content = $content. '<div class="option-group" style="display:none;">';
                        $content = $content. '<div>';
                        if($groupe->is_selected==1){
                          $content = $content. '<label>Min : </label><input type="number" value="'.$groupe->min.'" min="0" max="'.count($groupe->options).'" name="min[]" id="min'.$groupe->id.'" style="width: 37px;"><label>  Max : </label><input type="number" value="'.$groupe->max.'" min="1" max="'.count($groupe->options).'" name="max[]" id="max'.$groupe->id.'" style="width: 37px;">';
                        }
                        else{
                          $content = $content. '<label>Min : </label><input type="number" value="'.$groupe->min.'" min="0" max="'.count($groupe->options).'" name="" id="min'.$groupe->id.'" style="width: 37px;"><label>  Max : </label><input type="number" value="'.$groupe->max.'" min="1"  max="'.count($groupe->options).'" name="" id="max'.$groupe->id.'" style="width: 37px;">';
                        }
                        $content = $content. '</div>';
                        foreach($groupe->options as $option){
                          if($groupe->is_selected==1 && $option->is_selected==1){
                            $content = $content. '<div class="options"><div><input type="checkbox" checked value="'.$option->id.'" name="options[]" class="custom-checkbox option'.$groupe->id.'"><label>'.$option->name.'</label></div><div>  Prix : '.$option->prix.'</div></div>';
                          }
                          elseif($groupe->is_selected==1 && $option->is_selected==0){
                            $content = $content. '<div class="options"><div><input type="checkbox" value="'.$option->id.'" name="options[]" class="custom-checkbox option'.$groupe->id.'"><label>'.$option->name.'</label></div><div>  Prix : '.$option->prix.'</div></div>';
                          }
                          elseif($groupe->is_selected==0){
                            $content = $content. '<div class="options"><div><input type="checkbox" checked value="'.$option->id.'" name="" class="custom-checkbox option'.$groupe->id.'"><label>'.$option->name.'</label></div><div>  Prix : '.$option->prix.'</div></div>';
                          }
                        }
                        $content = $content. '</div>';
                        $content = $content. '</div>';
                        echo $content;
                      @endphp
                  @endforeach
                </div>
              </div>
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalAddNewOptions"><span class="fa fa-plus"></span> Add New Options</button>
            </div>
          </section>

        </div>
      </form>
    </div>
  </div>



  
        <!-- Modal Add New Supplement  -->

        <div class="modal fade" id="ModalAddNewSupplement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

              <div id="success1" class="alert alert-success" style="display: none;">
                <h4>
                  <i class="icon fa fa-info"></i>
                  Supplement Ajouté
                </h4>
              </div>


              <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong><i class="fa fa-glass"></i> Add Supplement</strong>
                  </div>
                  <div class="panel-body" style="padding:20px 0px 0px 0px">
                    <form class="form-horizontal" method="post" id="formAddSupplement" enctype="multipart/form-data" action="{{route('addsupplement')}}" >
                      @csrf
                      <div class="box-body" id="parent-form-area">
            
                        <div class="form-group header-group-0 " id="form-group-name" style="">
                          <label class="control-label col-sm-2">
                            Name
                            <span class="text-danger" title="This field is required">*</span>
                          </label>
                          <div class="col-sm-10">
                            <input type="text" title="Name" required="" class="form-control" name="name" id="name1" value="">
                            <div class="text-danger"></div>
                            <p class="help-block"></p>
                          </div>
                        </div>  

                        <div class="form-group header-group-0 " id="form-group-prix" style="">
                          <label class="control-label col-sm-2">
                            Prix
                            <span class="text-danger" title="This field is required">*</span>
                          </label>
                          <div class="col-sm-10">
                            <input type="text" title="Prix" min="0" required="" class="form-control" name="prix" id="prix1" value="">
                            <div class="text-danger"></div>
                            <p class="help-block"></p>
                          </div>
                        </div>           
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer" style="background: #F5F5F5;">
                        <div class="form-group">
                          <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-success" id="submitAddSupplement">Save</button>
                            </div>
                        </div>
                      </div>
                      <!-- /.box-footer-->
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <!-- Modal Add New Ingredient  -->

        <div class="modal fade" id="ModalAddNewIngredient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

              <div id="success2" class="alert alert-success" style="display: none;">
                <h4>
                  <i class="icon fa fa-info"></i>
                  Ingredient Ajouté
                </h4>
              </div>


              <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong><i class="fa fa-glass"></i> Add Ingredient</strong>
                  </div>
                  <div class="panel-body" style="padding:20px 0px 0px 0px">
                    <form class="form-horizontal" method="post" id="formAddIngredient" enctype="multipart/form-data" action="{{route('addingredient')}}" >
                      @csrf
                      {{-- action="http://144.91.100.67/flyfood/public/index.php/admin/product/addsupplement" --}}
                      <div class="box-body" id="parent-form-area">
                        
                        <div class="form-group header-group-0 " id="form-group-name" style="">
                          <label class="control-label col-sm-2">
                            Name
                            <span class="text-danger" title="This field is required">*</span>
                          </label>
                          <div class="col-sm-10">
                            <input type="text" title="Name" required="" class="form-control" name="name" id="name2" value="">
                            <div class="text-danger"></div>
                            <p class="help-block"></p>
                          </div>
                        </div>    
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer" style="background: #F5F5F5;">
                        <div class="form-group">
                          <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-success" id="submitAddIngredient">Save</button>
                            </div>
                        </div>
                      </div>
                      <!-- /.box-footer-->
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <!-- Modal Add New Options  -->

        <div class="modal fade" id="ModalAddNewOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div id="success3" class="alert alert-success" style="display: none;">
                <h4>
                  <i class="icon fa fa-info"></i>
                  Options groupe ajouté
                </h4>
              </div>
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong><i class="fa fa-glass"></i> Add Options</strong>
                  </div>
                  <div class="panel-body" style="padding:20px 0px 0px 0px">
                    <form class="form-horizontal" method="post" id="formAddOptions" enctype="multipart/form-data" action="{{route('addoptions')}}" >
                      @csrf
                      <div class="box-body" id="parent-form-area">
                        <div class="header-group-0 " id="form-group-name" style="">
                          <div class="option">
                            <div class="option-container" id="head1">
                              <div style="color : black !Important;">
                                <label>Nom du groupe : </label>
                                <input type="text" required name="newGroupeName" size="25">
                              </div>
                            </div>
                            <div id="newGroupOptions" class="option-group">
                              <div><label>Min : </label><input type="number" required name="newmin" value="0" min="0" max="20" style="width: 37px;"><label>  Max : </label><input type="number" required name="newmax" value="1" min="1" max="20" style="width: 37px;"></div>
                              <div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptions[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;"><button type="button" id="addoption" class="btn-primary" style="margin-left : 20px;">+</button></div>
                              <div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptions[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;"></div>
                            </div>
                          </div>
                        </div>    
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer" style="background: #F5F5F5;">
                        <div class="form-group">
                          <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-success" id="submitAddOptions">Save</button>
                            </div>
                        </div>
                      </div>
                      <!-- /.box-footer-->
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


@endsection

@push('bottom')
      <script src="{{asset('/js/jquery.nestable.js')}}"></script>
      <script src="{{asset('/js/jquery.steps.js')}}"></script>
      <script src="{{asset('/js/jquery.validate.js')}}"></script>
      <script>
        $( document ).ready(function() {
          $("#stepsDiv").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            onInit: () => {
              initPlugins();
            },
            onStepChanging: function (event, currentIndex, newIndex) {
              // Allways allow previous action even if the current form is not valid!
              if (currentIndex > newIndex) {
                  return true;
              }


              // Needed in some cases if the user went back (clean up)
              if (currentIndex < newIndex) {
                  // To remove error styles
                  $('#formAddProduct').find(".body:eq(" + newIndex + ") label.help-block").remove();
                  $('#formAddProduct').find(".body:eq(" + newIndex + ") .has-error").removeClass("has-error");
              }

              $('#formAddProduct').validate().settings.ignore = ":disabled,:hidden";

              let last_index = 3;
              if(newIndex == last_index && $('#formAddProduct').valid()) {
                  $('#submit_button_id').removeClass('hide');
                  $('#')
              }

                return $('#formAddProduct').valid();
            },
            onFinishing: function(event , currentIndex){
              $("#formAddProduct").submit();
            }

          });
        });

        function fillOptions(options_groupe){
          for (const element of options_groupe) {
            var content='';
            content += '<div class="option">';
            content += '<div class="option-container" id="group'+element.id+'">';
            content += '<div class="checkbox-label">';
            content += '<input  class="checkbox" type="checkbox" value="'+element.id+'" id="'+element.id+'" name="options_groupe[]"><label>'+element.name+'</label>';
            content += '</div>';
            content += '<div class="dropdown">';
            content += '<i class="fa fa-plus"></i>';
            content += '</div>';
            content += '</div>';
            content += '<div class="option-group" style="display:none;">';
            content += '<div>';
            content += '<label>Min : </label><input type="number" value="'+element.min+'" min="0" name="" id="min'+element.id+'" style="width: 37px;"><label>  Max : </label><input type="number" value="'+element.max+'" min="1"  name="" id="max'+element.id+'" style="width: 37px;">';
            content += '</div>';
            for (const option of element.options) {
              content += '<div class="options"><div><input type="checkbox" checked value="'+option.id+'" name="" class="custom-checkbox option'+element.id+'"><label>'+option.name+'</label></div><div>  Prix : '+option.prix+'</div></div>';
            }
            content += '</div>';
            content += '</div>';
            $('#options').append(content);
          }
        }

        function getRestoData(){
          var id_restaurant = {{$restaurant_id}};
          $.ajax({
            url: "{{route('getRestoData')}}",
            type: "POST",
            data: { id_restaurant: id_restaurant },
            success: function (response) {
              console.log(response.options_groupe);
              fillOptions(response.options_groupe);
            },
          });
        }

        function initPlugins(){
          
          $('#nest1').nestable({ maxDepth : 1 , group : 1 });
          $('#nest2').nestable({ maxDepth : 1 , group : 1 });
          $('#nest3').nestable({ maxDepth : 1 , group : 1 });
          $('#nest4').nestable({ maxDepth : 1 , group : 1 });

          $("#supplementJson").val(JSON.stringify( $('#nest1').nestable('serialize')));
          $("#ingredientJson").val(JSON.stringify( $('#nest3').nestable('serialize')));           

          $('#options').on('click','.checkbox', function() {
            var id = $(this).attr('id');
            if (this.checked){
              $("#min"+id).attr('name',"min[]");
              $("#max"+id).attr('name',"max[]");
              $(".option"+id).each(function(){
                $(this).attr('name',"options[]");
              });
            }
            else{
              $("#min"+id).attr('name',"");
              $("#max"+id).attr('name',"");
              $(".option"+id).each(function(){
                $(this).attr('name',"");
              });
            }
          });

          $('#options').on('click','.option-container',function(){
            var id = $(this).attr('id');
            if($('#'+id).next().hasClass('active')){
              $('#'+id).next().slideUp("fast");
              $('#'+id+' .dropdown i').addClass('fa-plus')
              $('#'+id+' .dropdown i').removeClass('fa-minus');
            }
            else{
              $('#'+id).next().slideDown("fast");
              $('#'+id+' .dropdown i').addClass('fa-minus')
              $('#'+id+' .dropdown i').removeClass('fa-plus');
            }
            $('#'+id).next().toggleClass('active');
          });

          $('#nest1,#nest2').on('change', function() {
            var json = $('#nest1').nestable('serialize');
            $("#supplementJson").val(JSON.stringify(json));
            if($('#nest1').children().length!=1){
              $('#nest1').children().remove();
              $('#nest1').prepend("<div class='dd-empty' style='background-color: #f5f5f56c'></div>");
            }
            if($('#nest2').children().length!=1){
              $('#nest2').children().remove();
              $('#nest2').prepend("<div class='dd-empty' style='background-color: #f5f5f56c'></div>");
            }
          });

          $('#nest3 , #nest4').on('change', function() {
            var json = $('#nest3').nestable('serialize');
            $("#ingredientJson").val(JSON.stringify(json));
            if($('#nest3').children().length!=1){
              $('#nest3').children().remove();
              $('#nest3').prepend("<div class='dd-empty' style='background-color: #f5f5f56c'></div>");
            }
            if($('#nest4').children().length!=1){
              $('#nest4').children().remove();
              $('#nest4').prepend("<div class='dd-empty' style='background-color: #f5f5f56c'></div>");
            }
          });

          $("#addAllSupplements").click(function() {
  
            if($('#nest2 .dd-list').children().length){

              if($('#nest1 .dd-list').children().length==0){
              $("#nest1 .dd-empty").remove();
              $("#nest1").append("<ol class='dd-list'></ol>");
              }
              
              $("#nest2 .dd-list li").each(function(){
                $("#nest1 .dd-list").append("<li class='dd-item'  data-id="+$(this).attr('data-id')+"><div class='dd-handle unselectable'>"+$(this).text()+"</div></li>");
                $("#nest2 .dd-list li").remove();
              });
              
              var json = $('#nest1').nestable('serialize');
              $("#supplementJson").val(JSON.stringify(json));
              $("#nest2 .dd-list").remove();
              $("#nest2").prepend("<div class='dd-empty' style='background-color: #f5f5f56c'></div>");
            }
          });

          $("#addAllIngredients").click(function() {
  
            if($('#nest4 .dd-list').children().length){

              if($('#nest3 .dd-list').children().length==0){
              $("#nest3 .dd-empty").remove();
              $("#nest3").append("<ol class='dd-list'></ol>");
              }
              
              $("#nest4 .dd-list li").each(function(){
                $("#nest3 .dd-list").append("<li class='dd-item'  data-id="+$(this).attr('data-id')+"><div class='dd-handle unselectable'>"+$(this).text()+"</div></li>");
                $("#nest4 .dd-list li").remove();
              });
              
              var json = $('#nest3').nestable('serialize');
              $("#ingredientJson").val(JSON.stringify(json));
              $("#nest4 .dd-list").remove();
              $("#nest4").prepend("<div class='dd-empty' style='background-color: #f5f5f56c'></div>");
            }
          });

        $('button[data-dismiss="modal"]').on('click',function(){
          $(this).closest("form").trigger("reset");
        });

        $("#formAddSupplement").submit(function (e) {
          e.preventDefault();
          var name = $("#name1").val();
          var id_restaurant = "{{$restaurant_id}}";
          var prix = $("#prix1").val();
          $.ajax({
            url: "{{route('addsupplement')}}",
            type: "POST",
            data: { name: name, id_restaurant: id_restaurant, prix: prix },
            success: function (response) {
              $("#submitAddSupplement").attr('disabled', 'disabled');
              $("#nest2 ol").append(
                `<li class="dd-item" data-id=${response.id}><div class="dd-handle unselectable">${response.name} | Prix : ${response.prix}</div></li>`
              );
              $("#success1").css("display", "block");
              setTimeout(() => {
                $("#formAddSupplement").trigger("reset");
                $("#ModalAddNewSupplement").modal("hide");
                $("#success1").css("display", "none");
                $("#submitAddSupplement").removeAttr('disabled');
              }, 2000);
            },
          });
        });

        $("#formAddIngredient").submit(function (e) {
          e.preventDefault();
          var name = $("#name2").val();
          var id_restaurant = "{{$restaurant_id}}";
          $.ajax({
            url: "{{route('addingredient')}}",
            type: "POST",
            data: { name: name, id_restaurant: id_restaurant },
            success: function (response) {
              $("#submitAddIngredient").attr('disabled', 'disabled');
              $("#nest4 ol").append(
                `<li class="dd-item" data-id=${response.id}><div class="dd-handle unselectable">${response.name} </div></li>`
              );
              $("#success2").css("display", "block");
              setTimeout(() => {
                $("#formAddIngredient").trigger("reset");
                $("#ModalAddNewIngredient").modal("hide");
                $("#success2").css("display", "none");
                $("#submitAddIngredient").removeAttr('disabled');
              }, 2000);
            },
          });
        });

        $('#addoption').on('click',function(){
          var n = $('#newGroupOptions').children().length;
          $('#newGroupOptions').append('<div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptions[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;"><button type="button" id="remove_option" class="btn-primary" style="background-color : red; margin-left : 20px;">-</button></div>');
        });

        $('#newGroupOptions').on('click','.add_option_div #remove_option',function(){
          console.log($(this).parent().attr("id"));
          $(this).parent().remove();
        });

        $("#formAddOptions").submit(function (e) {
          e.preventDefault();
          var id_restaurant = "{{$restaurant_id}}";
          var groupe_name = $('input[name="newGroupeName"]').val();
          var min = $('input[name="newmin"]').val();
          var max = $('input[name="newmax"]').val();
          var options = [];
          var prix = [];
          var i=0;
          $('input[name="newoptions[]"]').each(function(){
            options[i]=$(this).val();
            i++;
          });
          i=0;
          $('input[name="prix[]"]').each(function(){
            prix[i]=$(this).val();
            i++;
          });
          $.ajax({
            url: "{{route('addoptions')}}",
            type: "POST",
            data: { id_restaurant: id_restaurant, groupe_name: groupe_name, min: min, max : max, options : options, prix: prix },
            success: function (response) {
              console.log(response.groupe_id);
              $("#submitAddOptions").attr('disabled', 'disabled');
              var content=``;
              content += `<div class="option">`;
              content += `<div class="option-container" id="group${response.groupe_id}">`;
              content += `<div class="checkbox-label">`;
              content += `<input  class="checkbox" type="checkbox" value="${response.groupe_id}" id="${response.groupe_id}" name="options_groupe[]"><label>${response.groupe_name}</label>`;
              content += `</div>`;
              content += `<div class="dropdown">`;
              content += `<i class="fa fa-plus"></i>`;
              content += `</div>`;
              content += `</div>`;
              content += `<div class="option-group" style="display:none;">`;
              content += `<div>`;
              content += `<label>Min : </label><input type="number" value="${response.min}" min="0" name="" id="min${response.groupe_id}" style="width: 37px;"><label>  Max : </label><input type="number" value="${response.max}" min="1"  name="" id="max${response.groupe_id}" style="width: 37px;">`;
              content += `</div>`;
              for(var i=0;i<response.options_id.length;i++){
                content += `<div class="options"><div><input type="checkbox" value="${response.options_id[i]}" name="" class="custom-checkbox option${response.groupe_id}"><label>${response.options_name[i]}</label></div><div>  Prix : ${response.options_prix[i]}</div></div>`;
              }
              content += '</div>';
              content += '</div>';
              $('#options').append(content);
              $("#success3").css("display", "block");
              setTimeout(() => {
                $("#formAddOptions").trigger("reset");
                $("#ModalAddNewOptions").modal("hide");
                $("#success3").css("display", "none");
                $("#submitAddOptions").removeAttr('disabled');
              }, 2000);
            },
          });
        });

      }
    


        
      </script>
@endpush