@extends('crudbooster::admin_template')
@section('content')

  <style>
      .cover-photo {
        background: url("{{ asset($restaurant->cover_photo) }}");
        background-size: cover;
        background-repeat: no-repeat;
        height: 200px;
        position: relative;
        border-radius: 10px;
      }

      .main-photo {
        width: 120px;
        height: 120px;
        border-radius: 50%;
        object-fit: cover;
        position: absolute;
        left: calc(50% - 60px);
        top: 135px;
        border: 13px solid #ffffff;
    }

    #restaurant-name {
        text-align: center;
        font-size: 24px;
        font-weight: bold;
        margin-top: 55px;
        margin-bottom: 5px;
    }

      .section-block {
        margin: 70px 0;
      }

    #open-time {
        text-align: center;
        color: #808080;
        font-size: 15px;
    }

    .groupe {
        border: 1px solid #ddd;
        margin: 20px 0;
        border-radius: 7px;
    }

    .groupe-name::before {
        content: '\002B';
        position: absolute;
        right: 20px;
    }

    .groupe-name {
        background: #ffb751;
        padding: 10px;
        color: #fff;
        font-size: 20px;
        font-weight: bold;
        position: relative;
        cursor: pointer;
    }

    .groupe-name.active::before {
        content: "\2212";
    }

    .groupe-data {
        display: none;
        padding: 15px 25px;
    }

    .min-max {
        display: flex;
        justify-content: space-evenly;
    }
  </style>

<p><a title="Return" href="{{ URL::asset('index.php/admin/Resto')}}"><i class="fa fa-chevron-circle-left "></i>&nbsp; Back to restaurants list</a></p>

<div class="inner" style="background-color: #ffff;">
    <div class="panel-body">
        <div class="cover-photo">
            {{-- <img class="main-photo" src="{{ asset($restaurant->photo) }}" > --}}
            <a data-lightbox="roadtrip" href="{{ asset($restaurant->photo) }}"><img class="main-photo" src="{{ asset($restaurant->photo) }}"></a>
        </div>


        <h3 id="restaurant-name">{{$restaurant->name}}</h3>
        <div id="open-time">Open from {{$restaurant->open_at}} to {{$restaurant->close_at}} </div>


        <div class="section-block">
            <h3 class="text-center">Closed days</h3>
            <table class="table table-striped table-bordered text-center">
                <thead>
                <tr>
                    <th class="dd-handle">Monday</th>
                    <th class="dd-handle">Tuesday</th>
                    <th class="dd-handle">Wednesday</th>
                    <th class="dd-handle">Thursday</th>
                    <th class="dd-handle">Friday</th>
                    <th class="dd-handle">Saturday</th>
                    <th class="dd-handle">Sunday</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                            @if($restaurant->is_closed_day1)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                            @if($restaurant->is_closed_day2)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                            @if($restaurant->is_closed_day3)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                            @if($restaurant->is_closed_day4)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                            @if($restaurant->is_closed_day5)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                            @if($restaurant->is_closed_day6)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                            @if($restaurant->is_closed_day7)
                                <td><i class='fa fa-check'></i></td>
                            @else
                                <td></td>
                            @endif
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="section-block">
            <h3 class="text-center">Supplements</h3>
            <table class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($supplements as $supplement)
                        <tr>
                            <td >{{$supplement->name}}</td>
                            <td> {{$supplement->prix}} DT</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


        <div class="section-block">
            <h3 class="text-center">Ingredients</h3>
            <table class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ingredients as $ingredient)
                        <tr>
                            <td >{{$ingredient->name}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


        <div class="section-block">
            <h3 class="text-center" style="margin: 25px 0;">Option Groupes</h3>

            @foreach($options_groupe as $groupe)
                <div class="groupe">
                    <div class="groupe-name">
                        {{$groupe->name}}
                    </div>
                    <div class="groupe-data">
                        <div class="min-max">
                            <p><b>Minimum options :</b> {{$groupe->min}}</p>
                            <p><b>Maximum options:</b> {{$groupe->max}}</p>
                        </div>
                    
                        <div class="groupe-options">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <th>Option</th>
                                    <th>Price</th>
                                </thead>

                                <tbody>
                                    @foreach ($groupe->options as $option)
                                        <tr>
                                            <td>{{$option->name}}</td>
                                            <td>{{$option->prix}} DT</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach

            {{-- <table class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Min</th>
                        <th>Max</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($options_groupe as $groupe)
                        <tr>
                            <td >{{$groupe->name}}</td>
                            <td >{{$groupe->min}}</td>
                            <td >{{$groupe->max}}</td>
                        </tr>
                        <tr style="font-weight: bold">
                            <td>Options</td>
                            <td>Name</td>
                            <td>Price</td>
                        </tr>
                        @foreach ($groupe->options as $option)
                            <tr>
                                <td></td>
                                <td>{{$option->name}}</td>
                                <td>{{$option->prix}} DT</td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table> --}}
        </div>
        
    </div>
</div>
@endsection

@push('bottom')
    <script>
        $('.groupe-name').on('click', function() {
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).closest('.groupe').find('.groupe-data').slideUp('fast');
            } else {
                $(this).addClass('active');
                $(this).closest('.groupe').find('.groupe-data').slideDown('fast');
            }
        });
    </script>
@endpush