<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
<style>
    #table-detail tr td:first-child {
        font-weight: bold;
        width: 25%;
    }

    .option-container{
        display: flex;
        justify-content: space-between;
        align-items: center;
        background: #ffd491;
        padding: 11px 11px;
        border-radius:  5px 5px 0 0 ;
        color: #fff;
    }
 
    
    .option-group {
        padding: 20px;
        border: solid 1px #ccc;
        border-top: none;
        border-radius: 0 0 5px 5px;
        margin-bottom: 15px;
    }

    .option-group td {
        width: 50% !important;
    }

   
    #options{
        max-height: 100%;
        overflow: auto;
        width: 100%;
    }

    .option{
        margin-bottom: 5px;
    }

    .listing-item {
        display: flex;
        width: 100%;
        justify-content: space-between;
        background: #eee;
        padding: 10px;
        margin: 5px 0;
        border: 1px solid #aaa;
    }
</style>
@section('content')
  <!-- Your html goes here -->
<div>
    <p><a title="Return" href="{{ URL::asset('index.php/admin/produit')}}"><i class="fa fa-chevron-circle-left "></i>&nbsp; Back to product list</a></p>
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-glass"></i> Product display : {{$produit->name}}</strong>
        </div>
        <div class="panel-body" style="padding:20px 0px 0px 0px">
            <div class="box-body" id="parent-form-area">
                <div class="table-responsive">
                    <table id="table-detail" class="table table-striped">
                        <tbody>
                            <tr>
                                <td>Product Categorie</td>
                                <td>{{$name_categorie->name}}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>{{$produit->name}}</td>
                            </tr>
                            <tr>
                                <td>Photo</td>
                                <td><a data-lightbox="roadtrip" href="{{ URL::asset('')}}{{$produit->photo}}"><img style="max-width:150px" title="Image For Photo" src="{{ URL::asset('')}}{{$produit->photo}}"></a></td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>{{$produit->prix}} DT</td>
                            </tr>
                            <tr>
                                <td>Max Supplements</td>
                                <td>{{$produit->max_supplement}}</td>
                            </tr>
                            {{-- <tr>
                                <td>Available</td>
                                <td>@if ($produit->is_available==1)
                                        Yes <span class="fa fa-check text-green"></span>
                                    @else
                                        No <span class="fa fa-times text-red"></span>
                                    @endif
                                </td>
                            </tr> --}}
                            <tr>
                                <td>Supplements</td>
                                <td>
                                    @foreach($selected_supplements as $supplement)
                                            <div class="listing-item">
                                                <div>{{$supplement->name}}</div>
                                                <div>{{$supplement->prix}} DT</div>
                                            </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Ingredients to remove</td>
                                <td>
                                    @foreach($selected_ingredients as $ingredient)
                                    <div class="listing-item">{{$ingredient->name}}</div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Options</td>
                                <td>
                                    <div id="options">
                                        @foreach ($selected_options as $groupe)
                                            <div class="option">
                                                <div class="option-container">
                                                    <label>{{ $groupe->name }}</label>
                                                </div>

                                                <div class="option-group" style="display:block;"><div><label>Minimum options : </label><span style="width: 37px; margin-left: 10px; margin-right: 50px;">{{ $groupe->min }}</span><label>  Maximum options : </label><span style="width: 37px; margin: 0 10px;">{{ $groupe->max }}</span></div>
                                                    <table class="table table-bordered" style="margin-top: 10px">
                                                        <thead>
                                                            <tr><th style="background: #dddddd;">Option</th>  
                                                            <th style="background: #dddddd;">Price</th>  
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($groupe->options as $option)
                                                            <tr>
                                                                <td>{{ $option->name }}</td>
                                                                <td>{{ $option->prix }} DT</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <!-- /.box-body -->

            <div class="box-footer" style="background: #F5F5F5">
                <div class="form-group">
                    <label class="control-label col-sm-2"></label>
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>
            
            <!-- /.box-footer-->
        </div>
    </div>
</div>
@endsection
