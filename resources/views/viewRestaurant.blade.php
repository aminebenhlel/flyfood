@extends('crudbooster::admin_template')
<style>
    #table-detail tr td:first-child {
        font-weight: bold;
        width: 25%;
    }
    /* #table-detail tbody tr:nth-child(2n+1) {
        background-color: #0000001a;
    }    */
    .option-container{
        display: flex;
        justify-content: space-between;
        align-items: center;
        background: #5bafe0;
        padding: 11px 11px;
        border-radius:  5px 5px 0 0 ;
        color: #fff;
        cursor: pointer;
    }
    .checkbox-label{
        display: flex;
        align-items: center;
    }
    .checkbox-label label{
        margin-left: 8px;
        margin-bottom: 0;
    }
    .checkbox-label input{
        width: 20px;
        height: 20px;
        margin-top: 0;
    }
    .option-group{
        padding-left: 18px; 
        background: #5bafe054;
        border: solid 1px #5bafe0;
        border-top: none;
        border-radius: 0 0 5px 5px;
    }
    .custom-checkbox{
        margin-right: 10px !important;
    }
    #options{
        max-height: 100%;
        overflow: auto;
        width: 40%;
    }
    .option{
        margin-bottom: 5px;
    }
    .dd-handle { 
        width: 40%;
        display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
                border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
</style>
@section('content')
<div>
    <p><a title="Return" href="{{ URL::asset('index.php/admin/Resto')}}"><i class="fa fa-chevron-circle-left "></i>&nbsp; Retour au liste des restaurants</a></p>
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-glass"></i> Affichage du restaurant : {{$produit->name}}</strong>
        </div>
        <div class="panel-body" style="padding:20px 0px 0px 0px">
            <div class="box-body" id="parent-form-area">
                 <div class="table-responsive">
                  <table id="table-detail" class="table">
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{{$restaurant->name}}</td>
                            </tr>
                            <tr>
                                <td>Propriétaire</td>
                                <td>{{$restaurant_Proprietaire->name}}</td>
                            </tr>
                            <tr>
                                <td>Photo</td>
                                <td><a data-lightbox="roadtrip" href="{{ URL::asset('')}}{{$restaurant->photo}}"><img style="max-width:150px" title="Image For Photo" src="{{ URL::asset('')}}{{$restaurant->photo}}"></a></td>
                            </tr>
                            <tr>
                                <td>Cover Photo</td>
                                <td><a data-lightbox="roadtrip" href="{{ URL::asset('')}}{{$restaurant->cover_photo}}"><img style="max-width:150px" title="Image For Cover Photo" src="{{ URL::asset('')}}{{$restaurant->cover_photo}}"></a></td>
                            </tr>
                            <tr>
                                <td>Jours fermés</td>
                                <td>
                                    @if($restaurant->is_closed_day1)
                                        <div class="dd-handle">Lundi</div>
                                    @endif
                                    @if($restaurant->is_closed_day2)
                                        <div class="dd-handle">Mardi</div>
                                    @endif
                                    @if($restaurant->is_closed_day3)
                                        <div class="dd-handle">Mercredi</div>
                                    @endif
                                    @if($restaurant->is_closed_day4)
                                        <div class="dd-handle">Jeudi</div>
                                    @endif
                                    @if($restaurant->is_closed_day5)
                                        <div class="dd-handle">Vendredi</div>
                                    @endif
                                    @if($restaurant->is_closed_day6)
                                        <div class="dd-handle">Samedi</div>
                                    @endif
                                    @if($restaurant->is_closed_day7)
                                        <div class="dd-handle">Diamnche</div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Open At</td>
                                <td>{{$restaurant->open_at}}</td>
                            </tr>
                            <tr>
                                <td>Close At</td>
                                <td>{{$restaurant->close_at}}</td>
                            </tr>
                            <tr>
                                <td>Supplements</td>
                                <td>
                                    @foreach($supplements as $supplement)
                                            <div class="dd-handle">{{$supplement->name}} | Prix : {{$supplement->prix}} </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Ingrédients à enlever</td>
                                <td>
                                    @foreach($ingredients as $ingredient)
                                    <div class="dd-handle">{{$ingredient->name}}</div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Options</td>
                                <td>
                                    <div id="options">
                                        @foreach ($options_groupe as $groupe)
                                            @php
                                                $content = '';
                                                $content = $content. '<div class="option">';
                                                $content = $content. '<div class="option-container" id="group'.$groupe->id.'">';
                                                $content = $content. '<div class="checkbox-label">';
                                                $content = $content. '<input  class="checkbox" type="checkbox" checked><label>'.$groupe->name.'</label>';
                                                $content = $content. '</div>';
                                                $content = $content. '</div>';
                                                $content = $content. '<div class="option-group">';
                                                $content = $content. '<div>';
                                                $content = $content. '<label>Min : </label><input type="text" value="'.$groupe->min.'" style="width: 37px;" readonly><label>  Max : </label><input type="number" value="'.$groupe->max.'" style="width: 37px;" readonly>';
                                                $content = $content. '</div>';
                                                foreach($groupe->options as $option){
                                                    $content = $content. '<div class="options"><div><input type="checkbox" checked class="custom-checkbox"><label>'.$option->name.'</label></div><div>  Prix : '.$option->prix.'</div></div>';
                                                }
                                                $content = $content. '</div>';
                                                $content = $content. '</div>';
                                                echo $content;
                                            @endphp
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table> 
                   
                </div>
                {{-- <div class="col-md-6">
                    <p><label>Name : {{$restaurant->name}}<</label></p>
                    
                    <p><label>Propriétaire : {{$restaurant_Proprietaire->name}}</label></p>                           
                </div>
                  <div class="col-md-6">
                    
                    <a data-lightbox="roadtrip" href="{{ URL::asset('')}}{{$restaurant->photo}}"><img style="max-width:150px" title="Image For Photo" src="{{ URL::asset('')}}{{$restaurant->photo}}"></a>
                </div> --}}
            </div>
            
            <!-- /.box-body -->

            <div class="box-footer" style="background: #F5F5F5">
                <div class="form-group">
                    <label class="control-label col-sm-2"></label>
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>
            
            <!-- /.box-footer-->
        </div>
    </div>
</div>
@endsection
@push('bottom')

@endpush