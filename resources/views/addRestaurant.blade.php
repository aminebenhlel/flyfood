@extends('crudbooster::admin_template')
@push('head')
<style>
    #table-detail tbody tr td:first-child {
        font-weight: bold;
        width: 15%;
    }
    #table-detail tbody tr td:nth-child(2) {
        width: 50%;
        padding-right: 20%;
    }

    .option-container{
        display: flex;
        justify-content: space-between;
        align-items: center;
        background: #ffd491;
        padding: 11px 11px;
        border-radius:  5px 5px 0 0 ;
        color: #fff;
    }
    .checkbox-label{
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .checkbox-label label{
        margin-left: 8px;
        margin-bottom: 0;
    }
    .checkbox-label input{
        width: 20px;
        height: 20px;
        margin-top: 0;
    }

    .option-group{
      padding: 20px;
      border: solid 1px #ccc;
      border-top: none;
      border-radius: 0 0 5px 5px;
    }

    .option-group div {
        margin: 5px 0;
    }

    .custom-checkbox{
        margin-right: 10px !important;
    }
    #options{
        max-height: 100%;
        overflow: auto;
        width: 40%;
    }
    .option{
        margin-bottom: 5px;
    }
    .dd-handle { 
        min-width: 600px;
        width: 40%;
        display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
                border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }

    .tds {
        width: 100px !important;
        text-align: center!important;
        border: 1px solid rgba(0, 0, 0, 0.171);
        padding: 3px 0 !important;
    }

    th.tds {
      background: #ddd;
      padding: 3px 10px !important;
    }

    .checkbox-resize{
        width: 17px;
        height: 17px;
    }
    .supplement_add , .ingredient_add , .option_add{
        float: right;
        width: 190px;
    }

    .butttons {
      float: right;
    }

   .supplement_add span, .ingredient_add span, .option_add span {
        float: right;
        padding-top: 3px;
    }

    #submitButton {
      margin: 15px 0;
    }

    .fa.fa-trash {
        color: #dd4b39;
    }

    .option-group td {
        width: 50% !important;
    }
</style>
@endpush
@section('content')
<div>
  <p><a title="Return" href="{{ URL::asset('index.php/admin/Resto')}}"><i class="fa fa-chevron-circle-left "></i>&nbsp; Back to restaurants list</a></p>
  <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-list-alt"></i> {{ $page_title }}</strong>
        </div>
        <form class="form-horizontal" id="formEditRestaurant" enctype="multipart/form-data" method='post' action='{{route('addRestaurant')}}'>
            @csrf
            <div class="panel-body" style="padding:20px 0px 0px 0px">
                <div class="box-body" id="parent-form-area">
                    <div class="table-responsive">
                        <table id="table-detail" class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>
                                        <input type="text" title="Name" required="" class="form-control" name="name">
                                        <div class="text-danger"></div>
                                        <p class="help-block"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Owner</td>
                                    <td>
                                        <select style="width:100%" title="Owner" class="form-control select2-hidden-accessible" id="restaurant_Proprietaire" required="" name="restaurant_Proprietaire" tabindex="-1" aria-hidden="true">
                                            <option selected disabled>** Please choose an owner</option>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger"></div>
                                        <p class="help-block"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td>
                                        <div style="margin-top: 10px;">
                                            <input type="file" id="photo" title="Photo" class="form-control" name="photo" accept="image/*">
                                            <p class="help-block">File types support : JPG, JPEG, PNG, GIF, BMP</p>
                                            <div class="text-danger"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                  <td>Cover Photo</td>
                                  <td>
                                      <div style="margin-top: 10px;">
                                          <input type="file" id="photo" title="Photo" class="form-control" name="cover_photo" accept="image/*">
                                          <p class="help-block">File types support : JPG, JPEG, PNG, GIF, BMP</p>
                                          <div class="text-danger"></div>
                                      </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Categories</td>
                                  <td>
                                      <table>
                                          <tbody>
                                          <tr>
                                              @foreach ($categories as $item)
                                                  <th class="tds"><label>
                                                    {{$item->name}}</label></th>
                                              @endforeach
                                          </tr>
                                          <tr>
                                              @foreach ($categories as $item)
                                                  <td class="tds">
                                                    <input type="checkbox" name="categories[]" value="{{$item->id}}" class="checkbox-resize">
                                                  </td>
                                              @endforeach
                                          </tr>
                                      </table>
                                  </td>
                                </tr>
                                <tr>
                                    <td>Closed days</td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th class="tds"><label>Monday</label></th>
                                                <th class="tds"><label>Tuesday</label></th>
                                                <th class="tds"><label>Wednesday</label></th>
                                                <th class="tds"><label>Thursday</label></th>
                                                <th class="tds"><label>Friday</label></th>
                                                <th class="tds"><label>Saturday</label></th>
                                                <th class="tds"><label>Sunday</label></th>
                                            </tr>
                                            <tr>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="1" class="checkbox-resize">
                                                </td>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="2" class="checkbox-resize">
                                                </td>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="3" class="checkbox-resize">
                                                </td>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="4" class="checkbox-resize">
                                                </td>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="5" class="checkbox-resize">
                                                </td>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="6" class="checkbox-resize">
                                                </td>
                                                <td class="tds">
                                                    <input type="checkbox" name="jours[]" value="7" class="checkbox-resize">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Open At</td>
                                    <td>
                                        <input type="time" title="Open At" required="" class="form-control" name="open_at" id="open_at">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Close At</td>
                                    <td>
                                        <input type="time" title="Close At" required="" class="form-control" name="close_at" id="close_at">
                                    </td>
                                </tr>
                                <tr id="tr_supplements">
                                    <td>Supplements</td>
                                    <td id="td_supplements">
                                        <button type="button" class="btn btn-success btns valid supplement_add" data-toggle="modal" data-target="#ModalAddNewSupplement" aria-invalid="false">Add New Supplement <span class="fa fa-plus"></span></button>
                                    </td>
                                </tr>
                                <tr id="tr_ingredients">
                                  <td>Ingredients</td>
                                  <td id="td_ingredients">
                                      <button type="button" class="btn btn-success btns valid ingredient_add" data-toggle="modal" data-target="#ModalAddNewIngredient" aria-invalid="false">Add New Ingredient <span class="fa fa-plus"></span></button>
                                  </td>
                                </tr>
                                <tr id="tr_options">
                                    <td>Options</td>
                                    <td  id="td_options">
                                        <div id="options" style="width : 100%;">
                                            <button type="button" class="btn btn-success btns option_add" data-toggle="modal" data-target="#ModalAddNewOptions">Add New Options <span class="fa fa-plus"></span></button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <!-- /.box-body -->
            </div>

            <div class="text-center">
              <button class="btn btn-primary" id="submitButton" type="submit">Save <span class="fa fa-check"></span></button>
            </div>
        </form>
    </div>
</div>

        <!-- Modal Add New Supplement  -->

        <div class="modal fade" id="ModalAddNewSupplement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
  
  
                <div class="modal-header">
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Add Supplement</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formAddSupplement" enctype="multipart/form-data" action="{{route('addsupplement')}}" >
                        @csrf
                        <div class="box-body" id="parent-form-area">
              
                          <div class="form-group header-group-0 " id="form-group-name" style="">
                            <label class="control-label col-sm-2">
                              Name
                              <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-10">
                              <input type="text" title="Name" required="" class="form-control" maxlength="70" name="name" id="AddSupplementName" value="">
                              <div class="text-danger"></div>
                              <p class="help-block"></p>
                            </div>
                          </div>  
  
                          <div class="form-group header-group-0 " id="form-group-prix" style="">
                            <label class="control-label col-sm-2">
                              Price
                              <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-10">
                              <input type="number" title="Prix" min="0" required="" class="form-control" name="prix" id="AddSupplementPrix" value="" step="0.1">
                              <div class="text-danger"></div>
                              <p class="help-block"></p>
                            </div>
                          </div>           
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitAddSupplement">Save</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
  
                </div>
              </div>
            </div>
        </div>
  
          <!-- Modal Add New Ingredient  -->
  
          <div class="modal fade" id="ModalAddNewIngredient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Add Ingredient</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formAddIngredient" enctype="multipart/form-data" action="{{route('addingredient')}}" >
                        @csrf
                        {{-- action="http://144.91.100.67/flyfood/public/index.php/admin/product/addsupplement" --}}
                        <div class="box-body" id="parent-form-area">
                          
                          <div class="form-group header-group-0 " id="form-group-name" style="">
                            <label class="control-label col-sm-2">
                              Name
                              <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-10">
                              <input type="text" title="Name" required="" class="form-control" name="name" id="AddIngredientName" value="">
                              <div class="text-danger"></div>
                              <p class="help-block"></p>
                            </div>
                          </div>    
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitAddIngredient">Save</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
  
                </div>
              </div>
            </div>
          </div>
  
          <!-- Modal Add New Options  -->
  
          <div class="modal fade" id="ModalAddNewOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Add Options</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formAddOptions" enctype="multipart/form-data" action="{{route('addoptions')}}" >
                        @csrf
                        <div class="box-body" id="parent-form-area">
                          <div class="header-group-0 " id="form-group-name" style="">
                            <div class="option">
                              <div class="option-container">
                                <div style="color : black !Important;">
                                  <label>Group name : </label>
                                  <input type="text" required name="newGroupeName" size="25">
                                </div>
                              </div>
                              <div id="newGroupOptions1" class="option-group">
                                <div><label>Minimum options : </label><input type="number" required name="newmin" value="0" min="0" max="20" style="width: 37px; margin: 0 10px;"><label>  Maximum options : </label><input type="number" required name="newmax" value="1" min="1" max="20" style="width: 37px; margin: 0 10px;"></div>
                                <div class="add_option_div">
                                  <label style="margin-right : 10px;">Option :</label>
                                  <input type="text" required name="newoptions[]" placeholder="Option name" class="" size="20">
                                  
                                  <label style="margin-left : 10px;"> Price :</label>
                                  <input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;">

                                  <button type="button" class="btn-primary addoption1" style="margin-left : 20px;">
                                    <span class="fa fa-plus"></span>
                                  </button>
                                </div>

                                <div class="add_option_div">
                                  <label style="margin-right : 10px;">Option :</label>
                                  <input type="text" required name="newoptions[]" placeholder="Option name" class="" size="20">

                                  <label style="margin-left : 10px;"> Price :</label>
                                  <input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;">
                                </div>
                              </div>
                            </div>
                          </div>    
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitAddOptions">Save</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection
@push('bottom')
<script type="text/javascript">
    $( document ).ready(function() {
        
        $('.supplement_add').on('click',function(){
            $("#formAddSupplement").trigger("reset");
        });
        
        $('.ingredient_add').on('click',function(){
            $("#formAddIngredient").trigger("reset");
        });

        $('.option_add').on('click',function(){
            $("#formAddOptions").trigger("reset");
        });

        $("#formAddSupplement").submit(function (e) {
          e.preventDefault();
          var name = $("#AddSupplementName").val();
          var prix = $("#AddSupplementPrix").val();
          $("#submitAddSupplement").attr('disabled', 'disabled');
            var name = $("#AddSupplementName").val();
            var prix = $("#AddSupplementPrix").val();
            console.log(name,prix);
            $("#td_supplements button:last-child").before(`<div class="dd-handle" style="width : 100%;" div-id="" div-name="`+name+`" div-prix="`+prix+`"><input type="hidden" name="supplements_name[]" value="`+name+`"><input type="hidden" name="supplements_prix[]" value="`+prix+`"><label>`+name+` | Price : `+prix+`</label><div class="butttons"><a title="Delete" href="javascript:;" class="supplement_del"><i class="fa fa-trash"></i></a></div></div>`);
            $('.butttons').on('click','a.supplement_del',function(){
                var element = $(this).parent().parent();
                function ff(x){
                x.remove();
                }
                swal({ 
                title: "Are you sure ?",
                text: "You will not be able to recover this record data!",
                type: "warning",
                showCancelButton: true,   confirmButtonColor: "#ff0000",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                closeOnConfirm: true 
                },
                function(){
                ff(element);
                });
            });
            toastr['success']("Supplement added successfully");
              $("#ModalAddNewSupplement").modal("hide");
              $("#formAddSupplement").trigger("reset");
              $("#submitAddSupplement").removeAttr('disabled');
        });

        $("#formAddIngredient").submit(function (e) {
          e.preventDefault();
          var name = $("#AddIngredientName").val();
          $("#submitAddIngredient").attr('disabled', 'disabled');
          $("#td_ingredients button:last-child").before(`<div class="dd-handle" style="width : 100%;" div-id="" div-name="`+name+`""><input type="hidden" name="ingredients[]" value="`+name+`"><label>`+name+`</label><div class="butttons"><a title="Delete" href="javascript:;" class="ingredient_del"><i class="fa fa-trash"></i></a></div></div>`);
          $('.butttons').on('click','a.ingredient_del',function(){
                  var element = $(this).parent().parent();
                  function ff(x){
                      x.remove();
                  }
                  swal({ 
                      title: "Are you sure ?",
                      text: "You will not be able to recover this record data!",
                      type: "warning",
                      showCancelButton: true,   confirmButtonColor: "#ff0000",
                      confirmButtonText: "Yes!",
                      cancelButtonText: "No",
                      closeOnConfirm: true 
                  },
                  function(){
                        ff(element);
                  });
          });
          toastr['success']("Ingredient added successfully");
            $("#formAddIngredient").trigger("reset");
            $("#ModalAddNewIngredient").modal("hide");
            $("#submitAddIngredient").removeAttr('disabled');
        });

        $('.addoption1').on('click',function(){
          $('#newGroupOptions1').append(`
              <div class="add_option_div">
                <label style="margin-right : 10px;">Option :</label>
                <input type="text" required name="newoptions[]" placeholder="Option name" class="" size="20">
                
                <label style="margin-left : 10px;"> Price :</label>
                <input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;">

                <button type="button" id="remove_option" class="btn-danger" style="margin-left : 20px;">
                  <span class="fa fa-times"></span>
                </button>
              </div>
            `);
        });

        $('.addoption2').on('click',function(){
          var n = $('#newGroupOptions2').children().length;
          $('#newGroupOptions2').append(`
            <div class="add_option_div">
              <label style="margin-right : 10px;">Option :</label>
              <input type="text" required name="newoptionsUpdate[]" class="" size="20">
              
              <label style="margin-left : 10px;"> Price :</label>
              <input type="number" required name="prixUpdate[]" id="prix'+n+'" value="0" min="0" step="0.1" style="width: 50px;">

              <button type="button" id="remove_option" class="btn-danger" style="margin-left : 20px;">
                <span class="fa fa-times"></span>
              </button>
            </div>
          `);
        });

        $('#newGroupOptions1').on('click','.add_option_div #remove_option',function(){
          $(this).parent().remove();
        });

        $('#newGroupOptions2').on('click','.add_option_div #remove_option',function(){
          $(this).parent().remove();
        });

        $("#formAddOptions").submit(function (e) {
          e.preventDefault();
          var groupe_name = $('input[name="newGroupeName"]').val();
          var min = $('input[name="newmin"]').val();
          var max = $('input[name="newmax"]').val();
          var options = [];
          var prix = [];
          var i=0;
          $('input[name="newoptions[]"]').each(function(){
            options[i]=$(this).val();
            i++;
          });
          i=0;
          $('input[name="prix[]"]').each(function(){
            prix[i]=$(this).val();
            i++;
          });
          $("#submitAddOptions").attr('disabled', 'disabled');
            var content=``;
            content += `<div class="option">`;
            content += `<input type="hidden" name="options[]" value='{"name":"`+groupe_name+`","min":"`+min+`","max":"`+max+`","options":[`;
            for(var i=0;i<options.length;i++){
                content += `{"name":"`+options[i]+`","prix":"`+prix[i]+`"}`;
                if(i < options.length -1) {
                  content += ',';
                }
            }
            content += `] }' >`;
            content += `<div class="option-container" id="">`;
            content += `<div class="checkbox-label">`;
            content += `<label>`+groupe_name+`</label>`;
            content += `</div>`;
            content += `<div class="butttons"><a title="Delete" href="javascript:;" class="options_del"><i class="fa fa-trash"></i></a></div>`;
            content += `</div>`;
            content += `<div class="option-group" style="display:block;">`;
            content += `<div>`;
            content += `<label>Minimum options : </label><span style="width: 37px; margin-left: 10px; margin-right: 50px;" >`+min+`</span><label>  Maximum options : </label><span style="width: 37px; margin: 0 10px;" >`+max+`</span>`;
            content += `</div>`;
            content += `
              <table class="table">
                <thead>
                  <th style="background: #dddddd;">Option</th>  
                  <th style="background: #dddddd;">Price</th>  
                </thead>
                <tbody>
            `;
            for(var i=0;i<options.length;i++){
                content += `
                  <tr>
                    <td>`+options[i]+`</td>
                    <td>`+prix[i]+`</td>
                  </tr>`;
            }
            content += '</tbody>';
            content += '</table>';
            $("#td_options button:last-child").before(content);
            $('a.options_del').on('click',function(){
                console.log("1");
                  var element = $(this).parent().parent().parent();
                  function ff(x){
                      x.remove();
                  }
                  swal({ 
                      title: "Are you sure ?",
                      text: "You will not be able to recover this record data!",
                      type: "warning",
                      showCancelButton: true,   confirmButtonColor: "#ff0000",
                      confirmButtonText: "Yes!",
                      cancelButtonText: "No",
                      closeOnConfirm: true 
                  },
                  function(){
                        ff(element);
                  });
            });
            toastr['success']("Options added successfully");
              $("#formAddOptions").trigger("reset");
              $("#ModalAddNewOptions").modal("hide");
              $("#submitAddOptions").removeAttr('disabled');
        });
    });
    
</script>
@endpush