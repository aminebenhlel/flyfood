@extends('crudbooster::admin_template')
@push('head')
<style>
  #table-detail tbody tr td:first-child {
      font-weight: bold;
      width: 15%;
  }
  #table-detail tbody tr td:nth-child(2) {
      width: 50%;
      padding-right: 20%;
  }

  .option-container{
      display: flex;
      justify-content: space-between;
      align-items: center;
      background: #ffd491;
      padding: 11px 11px;
      border-radius:  5px 5px 0 0 ;
      color: #fff;
  }
  .checkbox-label{
      display: flex;
      align-items: center;
      justify-content: space-between;
  }
  .checkbox-label label{
      margin-left: 8px;
      margin-bottom: 0;
  }
  .checkbox-label input{
      width: 20px;
      height: 20px;
      margin-top: 0;
  }

  .option-group{
    padding: 20px;
    border: solid 1px #ccc;
    border-top: none;
    border-radius: 0 0 5px 5px;
  }

  .option-group div {
      margin: 5px 0;
  }

  .custom-checkbox{
      margin-right: 10px !important;
  }
  #options{
      max-height: 100%;
      overflow: auto;
      width: 40%;
  }
  .option{
      margin-bottom: 5px;
  }
  .dd-handle { 
      min-width: 600px;
      width: 40%;
      display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; border: 1px solid #ccc;
      background: #fafafa;
      background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
      background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
      background:         linear-gradient(top, #fafafa 0%, #eee 100%);
      -webkit-border-radius: 3px;
              border-radius: 3px;
      box-sizing: border-box; -moz-box-sizing: border-box;
  }

  .tds {
      width: 100px !important;
      text-align: center!important;
      border: 1px solid rgba(0, 0, 0, 0.171);
      padding: 3px 0 !important;
  }

  th.tds {
    background: #ddd;
    padding: 3px 10px !important;
  }

  .checkbox-resize{
      width: 17px;
      height: 17px;
  }
  .supplement_add , .ingredient_add , .option_add{
      float: right;
      width: 190px;
  }

  .butttons {
    float: right;
  }

 .supplement_add span, .ingredient_add span, .option_add span {
      float: right;
      padding-top: 3px;
  }

  #submitButton {
    margin: 15px 0;
  }

  .fa.fa-trash {
      color: #dd4b39;
  }

  .option-group td {
      width: 50% !important;
  }
</style>
@endpush
@section('content')
<div>
  <p><a title="Return" href="{{ URL::asset('index.php/admin/Resto')}}"><i class="fa fa-chevron-circle-left "></i>&nbsp; Back to restaurants list</a></p>
  <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-list-alt"></i> Edit restaurant : {{$restaurant->name}}</strong>
        </div>
        <form class="form-horizontal" id="formEditRestaurant" enctype="multipart/form-data" method='post' action='{{route('editRestaurant')}}'>
            @csrf
            <input type="hidden" name="id_restaurant" value="{{$restaurant->id}}">
            <div class="panel-body" style="padding:20px 0px 0px 0px">
                <div class="box-body" id="parent-form-area">
                    <div class="table-responsive">
                        <table id="table-detail" class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>
                                        <input type="text" title="Name" required="" class="form-control" name="name" value="{{$restaurant->name}}">
                                        <div class="text-danger"></div>
                                        <p class="help-block"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Owner</td>
                                    <td>
                                        <select style="width:100%" title="Propriétaire" class="form-control select2-hidden-accessible" id="restaurant_Proprietaire" required="" name="restaurant_Proprietaire" tabindex="-1" aria-hidden="true">
                                            @foreach($users as $user)
                                              @if ($user->name==$restaurant_Proprietaire->name)
                                                  <option value="{{ $user->id }}" selected="selected">{{ $user->name }}</option>
                                              @else
                                                  <option value="{{ $user->id }}" >{{ $user->name }}</option>
                                              @endif
                                            @endforeach
                                        </select>
                                        <div class="text-danger"></div>
                                        <p class="help-block"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td>
                                        <a data-lightbox="roadtrip" id="a_photo" href="{{ URL::asset('')}}{{$restaurant->photo}}"><img style="max-width:150px" id="img_photo" title="Image For Photo" src="{{ URL::asset('')}}{{$restaurant->photo}}"></a>
                                        <div style="margin-top: 10px;">
                                            <input type="file" id="photo" title="Photo" class="form-control" name="photo" accept="image/*">
                                            <p class="help-block">File types support : JPG, JPEG, PNG, GIF, BMP</p>
                                            <div class="text-danger"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                  <td>Cover Photo</td>
                                  <td>
                                      <a data-lightbox="roadtrip" id="a_photo" href="{{ URL::asset('')}}{{$restaurant->cover_photo}}"><img style="max-width:150px" id="img_photo" title="Image For Photo" src="{{ URL::asset('')}}{{$restaurant->cover_photo}}"></a>
                                      <div style="margin-top: 10px;">
                                          <input type="file" id="photo" title="Photo" class="form-control" name="cover_photo" accept="image/*">
                                          <p class="help-block">File types support : JPG, JPEG, PNG, GIF, BMP</p>
                                          <div class="text-danger"></div>
                                      </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Categories</td>
                                  <td>
                                      <table>
                                          <tbody>
                                          <tr>
                                              @foreach ($categories as $item)
                                                  <th class="tds"><label>
                                                    {{$item->name}}</label></th>
                                              @endforeach
                                          </tr>
                                          <tr>
                                              @foreach ($categories as $item)
                                                  <td class="tds">
                                                    <input type="checkbox" @if(in_array($item->id, $selected_categories)) checked="" @endif name="categories[]" value="{{$item->id}}" class="checkbox-resize">
                                                  </td>
                                              @endforeach
                                          </tr>
                                      </table>
                                  </td>
                                </tr>
                                <tr>
                                    <td>Closed days</td>
                                    <td>
                                        <table>
                                            <tbody>
                                            <tr>
                                              <th class="tds"><label>Monday</label></th>
                                              <th class="tds"><label>Tuesday</label></th>
                                              <th class="tds"><label>Wednesday</label></th>
                                              <th class="tds"><label>Thursday</label></th>
                                              <th class="tds"><label>Friday</label></th>
                                              <th class="tds"><label>Saturday</label></th>
                                              <th class="tds"><label>Sunday</label></th>
                                            </tr>
                                            <tr>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day1==1)
                                                    <input type="checkbox" name="jours[]" value="1" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="1" class="checkbox-resize">
                                                    @endif
                                                </td>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day2==1)
                                                    <input type="checkbox" name="jours[]" value="2" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="2" class="checkbox-resize">
                                                    @endif
                                                </td>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day3==1)
                                                    <input type="checkbox" name="jours[]" value="3" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="3" class="checkbox-resize">
                                                    @endif
                                                </td>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day4==1)
                                                    <input type="checkbox" name="jours[]" value="4" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="4" class="checkbox-resize">
                                                    @endif
                                                </td>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day5==1)
                                                    <input type="checkbox" name="jours[]" value="5" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="5" class="checkbox-resize">
                                                    @endif
                                                </td>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day6==1)
                                                    <input type="checkbox" name="jours[]" value="6" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="6" class="checkbox-resize">
                                                    @endif
                                                </td>
                                                <td class="tds">
                                                    @if($restaurant->is_closed_day7==1)
                                                    <input type="checkbox" name="jours[]" value="7" class="checkbox-resize" checked>
                                                    @else
                                                    <input type="checkbox" name="jours[]" value="7" class="checkbox-resize">
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Open At</td>
                                    <td>
                                        <input type="time" title="Open At" required="" class="form-control" name="open_at" id="open_at" value="{{$restaurant->open_at}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Close At</td>
                                    <td>
                                        <input type="time" title="Close At" required="" class="form-control" name="close_at" id="close_at" value="{{$restaurant->close_at}}">
                                    </td>
                                </tr>
                                <tr id="tr_supplements">
                                    <td>Supplements</td>
                                    <td id="td_supplements">
                                        @foreach($supplements as $supplement)
                                                <div class="dd-handle" style="width : 100%;" div-id="{{$supplement->id}}" div-name="{{$supplement->name}}" div-prix="{{$supplement->prix}}">
                                                    <label>{{$supplement->name}} | Prix : {{$supplement->prix}}</label>
                                                    <div class="butttons">
                                                        <a title="Delete" href="javascript:;" class="supplement_del">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                        <a style="margin-left: 7px;" title="Edit" href="" data-toggle="modal" data-target="#ModalUpdateSupplement" class="supplement_upd" id="{{$supplement->id}}">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                        @endforeach
                                        <button type="button" class="btn btn-success btns valid supplement_add" data-toggle="modal" data-target="#ModalAddNewSupplement" aria-invalid="false">Add New Supplement <span class="fa fa-plus"></span></button>
                                    </td>
                                </tr>
                                <tr id="tr_ingredients">
                                  <td>Ingredients</td>
                                  <td id="td_ingredients">
                                      @foreach($ingredients as $ingredient)
                                              <div class="dd-handle" style="width : 100%;" div-id="{{$ingredient->id}}" div-name="{{$ingredient->name}}">
                                                  <label>{{$ingredient->name}}</label>
                                                  <div class="butttons">
                                                      <a title="Delete" href="javascript:;" class="ingredient_del">
                                                          <i class="fa fa-trash"></i>
                                                      </a>
                                                      <a style="margin-left: 7px;" title="Edit" href="" data-toggle="modal" data-target="#ModalUpdateIngredient" class="ingredient_upd" id="{{$ingredient->id}}">
                                                          <i class="fa fa-pencil"></i>
                                                      </a>
                                                  </div>
                                              </div>
                                      @endforeach
                                      <button type="button" class="btn btn-success btns valid ingredient_add" data-toggle="modal" data-target="#ModalAddNewIngredient" aria-invalid="false">Add New Ingredient <span class="fa fa-plus"></span></button>
                                  </td>
                                </tr>
                                <tr id="tr_options">
                                    <td>Options</td>
                                    <td  id="td_options">
                                        <div id="options" style="width : 100%;">
                                            @foreach ($options_groupe as $groupe)
                                                @php
                                                    $content = '';
                                                    $content = $content. '<div class="option">';
                                                    $content = $content. '<div class="option-container" id="group'.$groupe->id.'">';
                                                    $content = $content. '<div class="checkbox-label">';
                                                    $content = $content. '<input  class="checkbox" type="checkbox" checked><label>'.$groupe->name.'</label>';
                                                    $content = $content. '</div>';
                                                    $content = $content. '<div class="butttons"><a title="Delete" href="javascript:;" class="options_del"><i class="fa fa-trash"></i></a><a style="margin-left: 7px;" title="Edit" href="" data-toggle="modal" data-target="#ModalUpdateOptions" class="options_upd" id="'.$groupe->id.'"><i class="fa fa-pencil"></i></a></div>';
                                                    $content = $content. '</div>';
                                                    $content = $content. '<div class="option-group">';
                                                    $content = $content. '<div>';
                                                    $content = $content. '<label>Min : </label><input type="text" value="'.$groupe->min.'" style="width: 37px;" readonly><label>  Max : </label><input type="number" value="'.$groupe->max.'" style="width: 37px;" readonly>';
                                                    $content = $content. '</div>';
                                                    foreach($groupe->options as $option){
                                                        $content = $content. '<div class="option"><div><input type="checkbox" checked class="custom-checkbox"><label>'.$option->name.'</label></div><div>  Prix : '.$option->prix.'</div></div>';
                                                    }
                                                    $content = $content. '</div>';
                                                    $content = $content. '</div>';
                                                    echo $content;
                                                @endphp
                                            @endforeach
                                            <button type="button" class="btn btn-success btns option_add" data-toggle="modal" data-target="#ModalAddNewOptions">Add New Options <span class="fa fa-plus"></span></button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <!-- /.box-body -->

                <div class="text-center">
                  <button class="btn btn-primary" id="submitButton" type="submit">Save <span class="fa fa-check"></span></button>
                </div>
        </form>
    </div>
</div>

        <!-- Modal Add New Supplement  -->

        <div class="modal fade" id="ModalAddNewSupplement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
  
                <div id="success1" class="alert alert-success" style="display: none;">
                  <h4>
                    <i class="icon fa fa-info"></i>
                  </h4>
                </div>
  
  
                <div class="modal-header">
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Add Supplement</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formAddSupplement" enctype="multipart/form-data" action="{{route('addsupplement')}}" >
                        @csrf
                        <div class="box-body" id="parent-form-area">
              
                          <div class="form-group header-group-0 " id="form-group-name" style="">
                            <label class="control-label col-sm-2">
                              Name
                              <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-10">
                              <input type="text" title="Name" required="" class="form-control" maxlength="70" name="name" id="AddSupplementName" value="">
                              <div class="text-danger"></div>
                              <p class="help-block"></p>
                            </div>
                          </div>  
  
                          <div class="form-group header-group-0 " id="form-group-prix" style="">
                            <label class="control-label col-sm-2">
                              Prix
                              <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-10">
                              <input type="number" title="Prix" min="0" required="" class="form-control" name="prix" id="AddSupplementPrix" value="" step="0.1">
                              <div class="text-danger"></div>
                              <p class="help-block"></p>
                            </div>
                          </div>           
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitAddSupplement">Save</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
  
                </div>
              </div>
            </div>
        </div>

        <!-- Modal Update Supplement  -->

        <div class="modal fade" id="ModalUpdateSupplement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

              <div id="success4" class="alert alert-success" style="display: none;">
                <h4>
                  <i class="icon fa fa-info"></i>
                </h4>
              </div>


              <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong><i class="fa fa-glass"></i> Update Supplement</strong>
                  </div>
                  <div class="panel-body" style="padding:20px 0px 0px 0px">
                    <form class="form-horizontal" method="post" id="formUpdateSupplement" action="{{route('updatesupplement')}}" >
                      @csrf
                      <input type="hidden" name="oldname" value="" id="UpdateSupplementId">
                      <div class="box-body" id="parent-form-area">
            
                        <div class="form-group header-group-0 " id="form-group-name" style="">
                          <label class="control-label col-sm-2">
                            Name
                            <span class="text-danger" title="This field is required">*</span>
                          </label>
                          <div class="col-sm-10">
                            <input type="text" title="newname" required="" class="form-control" maxlength="70" name="name" id="UpdateSupplementNewName" value="">
                            <div class="text-danger"></div>
                            <p class="help-block"></p>
                          </div>
                        </div>  

                        <div class="form-group header-group-0 " id="form-group-prix" style="">
                          <label class="control-label col-sm-2">
                            Prix
                            <span class="text-danger" title="This field is required">*</span>
                          </label>
                          <div class="col-sm-10">
                            <input type="number" title="Prix" min="0" required="" class="form-control" name="prix" id="UpdateSupplementPrix" value="" step="0.1">
                            <div class="text-danger"></div>
                            <p class="help-block"></p>
                          </div>
                        </div>           
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer" style="background: #F5F5F5;">
                        <div class="form-group">
                          <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-success" id="submitUpdateSupplement">Update</button>
                            </div>
                        </div>
                      </div>
                      <!-- /.box-footer-->
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
  
          <!-- Modal Add New Ingredient  -->
  
          <div class="modal fade" id="ModalAddNewIngredient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
  
                <div id="success2" class="alert alert-success" style="display: none;">
                  <h4>
                    <i class="icon fa fa-info"></i>
                    Ingredient Ajouté
                  </h4>
                </div>
  
  
                <div class="modal-header">
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Add Ingredient</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formAddIngredient" enctype="multipart/form-data" action="{{route('addingredient')}}" >
                        @csrf
                        {{-- action="http://144.91.100.67/flyfood/public/index.php/admin/product/addsupplement" --}}
                        <div class="box-body" id="parent-form-area">
                          
                          <div class="form-group header-group-0 " id="form-group-name" style="">
                            <label class="control-label col-sm-2">
                              Name
                              <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-10">
                              <input type="text" title="Name" required="" class="form-control" name="name" id="AddIngredientName" value="">
                              <div class="text-danger"></div>
                              <p class="help-block"></p>
                            </div>
                          </div>    
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitAddIngredient">Save</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
  
                </div>
              </div>
            </div>
          </div>

        <!-- Modal Update Ingredient  -->

        <div class="modal fade" id="ModalUpdateIngredient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

              <div id="success5" class="alert alert-success" style="display: none;">
                <h4>
                  <i class="icon fa fa-info"></i>
                </h4>
              </div>


              <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong><i class="fa fa-glass"></i> Update Ingredient</strong>
                  </div>
                  <div class="panel-body" style="padding:20px 0px 0px 0px">
                    <form class="form-horizontal" method="post" id="formUpdateIngredient" action="{{route('updateingredient')}}" >
                      @csrf
                      <input type="hidden" name="oldname" value="" id="UpdateIngredientId">
                      <div class="box-body" id="parent-form-area">
            
                        <div class="form-group header-group-0 " id="form-group-name" style="">
                          <label class="control-label col-sm-2">
                            Name
                            <span class="text-danger" title="This field is required">*</span>
                          </label>
                          <div class="col-sm-10">
                            <input type="text" title="newname" required="" class="form-control" maxlength="70" name="name" id="UpdateIngredientNewName" value="">
                            <div class="text-danger"></div>
                            <p class="help-block"></p>
                          </div>
                        </div>           
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer" style="background: #F5F5F5;">
                        <div class="form-group">
                          <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-success" id="submitUpdateIngredient">Update</button>
                            </div>
                        </div>
                      </div>
                      <!-- /.box-footer-->
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div>
      </div>
  
          <!-- Modal Add New Options  -->
  
          <div class="modal fade" id="ModalAddNewOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div id="success3" class="alert alert-success" style="display: none;">
                  <h4>
                    <i class="icon fa fa-info"></i>
                  </h4>
                </div>
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Add Options</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formAddOptions" enctype="multipart/form-data" action="{{route('addoptions')}}" >
                        @csrf
                        <div class="box-body" id="parent-form-area">
                          <div class="form-group header-group-0 " id="form-group-name" style="">
                            <div class="option">
                              <div class="option-container">
                                <div style="color : black !Important;">
                                  <label>Nom du groupe : </label>
                                  <input type="text" required name="newGroupeName" size="25">
                                </div>
                              </div>
                              <div id="newGroupOptions1" class="option-group">
                                <div><label>Min : </label><input type="number" required name="newmin" value="0" min="0" max="20" style="width: 37px;"><label>  Max : </label><input type="number" required name="newmax" value="1" min="1" max="20" style="width: 37px;"></div>
                                <div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptions[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;"><button type="button" class="btn-primary addoption1" style="margin-left : 20px;">+</button></div>
                                <div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptions[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;"></div>
                              </div>
                            </div>
                          </div>    
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitAddOptions">Save</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal Update Options  -->
  
          <div class="modal fade" id="ModalUpdateOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div id="success6" class="alert" style="display: none;">
                  <h4>
                    <i class="icon fa fa-info"></i>
                  </h4>
                </div>
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <strong><i class="fa fa-glass"></i> Update Options</strong>
                    </div>
                    <div class="panel-body" style="padding:20px 0px 0px 0px">
                      <form class="form-horizontal" method="post" id="formUpdateOptions" enctype="multipart/form-data" action="{{route('updateOptions')}}" >
                        @csrf
                        <input type="hidden" name="idGroupeUpdate" value="">
                        <input type="hidden" name="oldGroupeNameUpdate" value="">
                        <div class="box-body" id="parent-form-area">
                          <div class="form-group header-group-0 " id="form-group-name" style="">
                            <div class="option">
                              <div class="option-container">
                                <div style="color : black !Important;">
                                  <label>Nom du groupe : </label>
                                  <input type="text" required name="newGroupeNameUpdate" size="25">
                                </div>
                              </div>
                              <div id="newGroupOptions2" class="option-group">
                                <div><label>Min : </label><input type="number" required name="newminUpdate" value="0" min="0" max="20" style="width: 37px;"><label>  Max : </label><input type="number" required name="newmaxUpdate" value="1" min="1" max="20" style="width: 37px;"></div>
                                <div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptionsUpdate[]" id="newoption1" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prixUpdate[]" id="prix1"  value="0" min="0" step="0.1" style="width: 50px;"><button type="button" class="btn-primary addoption2" style="margin-left : 20px;">+</button></div>
                                <div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptionsUpdate[]" id="newoption2" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prixUpdate[]" id="prix2" value="0" min="0" step="0.1" style="width: 50px;"></div>
                              </div>
                            </div>
                          </div>    
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer" style="background: #F5F5F5;">
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                              <div class="col-sm-10">
                                <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="submitUpdateOptions">Update</button>
                              </div>
                          </div>
                        </div>
                        <!-- /.box-footer-->
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection
@push('bottom')
<script type="text/javascript">
    $( document ).ready(function() {

        $('.butttons').on('click','a.supplement_del',function(){
                var element = $(this).parent().parent();
                var id = $(this).parent().parent().attr('div-id');
                function ff(x){
                    x.remove();
                }
                swal({ 
                    title: "Are you sure ?",
                    text: "You will not be able to recover this record data!",
                    type: "warning",
                    showCancelButton: true,   confirmButtonColor: "#ff0000",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    closeOnConfirm: true 
                },
                function(){
                  $.ajax({
                    url: "{{route('delsupplement')}}",
                    type: "POST",
                    data: { id: id },
                    success: function (response) {
                      ff(element);
                    },
                  });
                });
        });
        
        $('.butttons').on('click','a.supplement_upd',function(){
            $('#UpdateSupplementNewName').attr('value',$(this).parent().parent().attr('div-name'));
            $('#UpdateSupplementPrix').attr('value',$(this).parent().parent().attr('div-prix'));
            $('#UpdateSupplementId').attr('value',$(this).parent().parent().attr('div-id'));
        });
        
        $('.supplement_add').on('click',function(){
            $("#formAddSupplement").trigger("reset");
        });

        $('.butttons').on('click','a.ingredient_del',function(){
                var element = $(this).parent().parent();
                var id = $(this).parent().parent().attr('div-id');
                console.log(id);
                function ff(x){
                    x.remove();
                }
                swal({ 
                    title: "Are you sure ?",
                    text: "You will not be able to recover this record data!",
                    type: "warning",
                    showCancelButton: true,   confirmButtonColor: "#ff0000",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    closeOnConfirm: true 
                },
                function(){
                  $.ajax({
                    url: "{{route('delingredient')}}",
                    type: "POST",
                    data: { id: id },
                    success: function (response) {
                      ff(element);
                    },
                  });
                });
        });
        
        $('.butttons').on('click','a.ingredient_upd',function(){
            $('#UpdateIngredientNewName').attr('value',$(this).parent().parent().attr('div-name'));
            $('#UpdateIngredientId').attr('value',$(this).parent().parent().attr('div-id'));
        });
        
        $('.ingredient_add').on('click',function(){
            $("#formAddIngredient").trigger("reset");
        });

        $('.butttons').on('click','a.options_del',function(){
                var element = $(this).parent().parent().parent();
                var id = parseInt($(this).parent().parent().attr('id').substring(5));
                console.log(id);
                function ff(x){
                    x.remove();
                }
                swal({ 
                    title: "Are you sure ?",
                    text: "You will not be able to recover this record data!",
                    type: "warning",
                    showCancelButton: true,   confirmButtonColor: "#ff0000",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    closeOnConfirm: true 
                },
                function(){
                  $.ajax({
                    url: "{{route('delOptions')}}",
                    type: "POST",
                    data: { id: id },
                    success: function () {
                      ff(element);
                    },
                  });
                });
        });

        $('.butttons').on('click','a.options_upd',function(){
            var n = $(this).parent().parent().parent().children().eq(1).children().length-1;
            var n2 = $("#newGroupOptions2").children().length;
            if(n2>3){
              for(var i=3;i<n2;i++){
                $("#newGroupOptions2").children().eq(3).remove();
              }
            }
            $('input[name="idGroupeUpdate"]').attr('value',parseInt($(this).parent().parent().attr('id').substring(5)));
            $('input[name="newGroupeNameUpdate"] , input[name="oldGroupeNameUpdate"]').attr('value',$(this).parent().parent().children().eq(0).children().eq(1).text());
            $('input[name="newminUpdate"]').attr('value',$(this).parent().parent().parent().children().eq(1).children().eq(0).children().eq(1).attr('value'));
            $('input[name="newmaxUpdate"]').attr('value',$(this).parent().parent().parent().children().eq(1).children().eq(0).children().eq(3).attr('value'));
            for(var i=1;i<3;i++){
              $('#newoption'+i).attr('value',$(this).parent().parent().parent().children().eq(1).children().eq(i).children().eq(0).children().eq(1).text());
              $('#prix'+i).attr('value',parseFloat($(this).parent().parent().parent().children().eq(1).children().eq(i).children().eq(1).text().substring(8)));
            }
            for(var i=3;i<=n;i++){
              $('#newGroupOptions2').append('<div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptionsUpdate[]" id="newoption'+i+'" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prixUpdate[]" id="prix'+i+'" value="0" min="0" step="0.1" style="width: 50px;"><button type="button" id="remove_option" class="btn-primary" style="background-color : red; margin-left : 20px;">-</button></div>');
              $('#newoption'+i).attr('value',$(this).parent().parent().parent().children().eq(1).children().eq(i).children().eq(0).children().eq(1).text());
              $('#prix'+i).attr('value',parseFloat($(this).parent().parent().parent().children().eq(1).children().eq(i).children().eq(1).text().substring(8)));
            }
        });

        $('.option_add').on('click',function(){
            $("#formAddOptions").trigger("reset");
        });

        $('#options').on('click','.checkbox', function() {
          //  console.log('1');
          //   var id = $(this).attr('id');
          //   if (this.checked){
          //     $("#min"+id).attr('name',"min[]");
          //     $("#max"+id).attr('name',"max[]");
          //     $(".option"+id).each(function(){
          //       $(this).attr('name',"options[]");
          //     });
          //   }
          //   else{
          //     $("#min"+id).attr('name',"");
          //     $("#max"+id).attr('name',"");
          //     $(".option"+id).each(function(){
          //       $(this).attr('name',"");
          //     });
          //   }
        });

        $('#options').on('click','.option-container',function(){
            // var id1 = $(this).children().children();
            // id = $(this).attr('id');
            // if($('#'+id).next().hasClass('active')){
            //     $('#'+id).next().slideUp("fast");
            //     $('#'+id+' .dropdown i').addClass('fa-plus')
            //     $('#'+id+' .dropdown i').removeClass('fa-minus');
            // }
            // else{
            //     $('#'+id).next().slideDown("fast");
            //     $('#'+id+' .dropdown i').addClass('fa-minus')
            //     $('#'+id+' .dropdown i').removeClass('fa-plus');
            // }
            // $('#'+id).next().toggleClass('active');
        });

        $("#formUpdateSupplement").submit(function (e) {
          e.preventDefault();
          var newname = $("#UpdateSupplementNewName").val();
          var id = $("#UpdateSupplementId").val();
          var id_restaurant = "{{$restaurant->id}}";
          var prix = $("#UpdateSupplementPrix").val();
          $.ajax({
            url: "{{route('updatesupplement')}}",
            type: "POST",
            data: { newname: newname, id: id, id_restaurant: id_restaurant, prix: prix },
            success: function (response) {
              if(response.exist==0){
                    $("#submitUpdateSupplement").attr('disabled', 'disabled');
                    $('#td_supplements div[div-id="'+id+'"]').children(":first").text(response.name+' | Prix : '+response.prix);
                    $('#td_supplements div[div-id="'+id+'"]').attr('div-prix',response.prix);
                    $('#td_supplements div[div-id="'+id+'"]').attr('div-name',response.name);
                    $("#success4").children().text("Ce supplément a été mis à jour");
                    $("#success4").attr("class","alert alert-success");
                    $("#success4").css("display", "block");
                    setTimeout(() => {
                        $("#ModalUpdateSupplement").modal("hide");
                        $("#formUpdateSupplement").trigger("reset");
                        $("#success4").css("display","none");
                        $("#submitUpdateSupplement").removeAttr('disabled');
                    }, 3000);
              }
              else{
                    $("#success4").children().text("Il Existe déja un supplement identique");
                    $("#success4").attr("class","alert alert-danger");
                    $("#success4").css("display", "block");
                    setTimeout(() => {
                        $("#success4").css("display", "none");
                    }, 3000);
              }
            },
          });
        });

        $("#formAddSupplement").submit(function (e) {
          e.preventDefault();
          var name = $("#AddSupplementName").val();
          var id_restaurant = "{{$restaurant->id}}";
          var prix = $("#AddSupplementPrix").val();
          $.ajax({
            url: "{{route('addsupplement')}}",
            type: "POST",
            data: { name: name, id_restaurant: id_restaurant, prix: prix },
            success: function (response) {
                if(response.exist==0){
                    $("#submitAddSupplement").attr('disabled', 'disabled');
                    $("#td_supplements button:last-child").before(`<div class="dd-handle" style="width : 100%;" div-id="`+response.id+`" div-name="`+name+`" div-prix="`+prix+`"><label>${response.name} | Prix : ${response.prix}</label><div class="butttons"><a title="Delete" href="javascript:;" class="supplement_del"><i class="fa fa-trash"></i></a><a style="margin-left: 7px;" title="Edit" href="javascript:;" data-toggle="modal" data-target="#ModalUpdateSupplement" class="supplement_upd" id="`+response.id+`"><i class="fa fa-pencil"></i></a></div></div>`);
                    $('.butttons').on('click','a.supplement_del',function(){
                      var element = $(this).parent().parent();
                      var id = $(this).parent().parent().attr('div-id');
                      console.log(id);
                      function ff(x){
                      x.remove();
                      }
                      swal({ 
                      title: "Are you sure ?",
                      text: "You will not be able to recover this record data!",
                      type: "warning",
                      showCancelButton: true,   confirmButtonColor: "#ff0000",
                      confirmButtonText: "Yes!",
                      cancelButtonText: "No",
                      closeOnConfirm: true 
                      },
                      function(){
                      $.ajax({
                      url: "{{route('delsupplement')}}",
                      type: "POST",
                      data: { id: id },
                      success: function (response) {
                      ff(element);
                      },
                      });
                      });
                    });
                    $('.butttons').on('click','a.supplement_upd',function(){
                      $('#UpdateSupplementNewName').attr('value',$(this).parent().parent().attr('div-name'));
                      $('#UpdateSupplementPrix').attr('value',$(this).parent().parent().attr('div-prix'));
                      $('#UpdateSupplementId').attr('value',$(this).parent().parent().attr('div-id'));
                      console.log($(this).parent().parent().attr('div-name'));
                      console.log($(this).parent().parent().attr('div-prix'));
                    });
                    $("#success1").children().text("Supplement Ajouté")
                    $("#success1").attr("class","alert alert-success");
                    $("#success1").css("display", "block");
                    setTimeout(() => {
                        $("#ModalAddNewSupplement").modal("hide");
                        $("#formAddSupplement").trigger("reset");
                        $("#success1").css("display","none");
                        $("#submitAddSupplement").removeAttr('disabled');
                    }, 3000);
                }
                else{
                    $("#success1").children().text("Supplement Existe déja");
                    $("#success1").attr("class","alert alert-danger");
                    $("#success1").css("display", "block");
                    setTimeout(() => {
                        $("#success1").css("display", "none");
                    }, 3000);
                }
            },
          });
        });

        $("#formAddIngredient").submit(function (e) {
          e.preventDefault();
          var name = $("#AddIngredientName").val();
          var id_restaurant = "{{$restaurant->id}}";
          console.log(name+" | "+id_restaurant);
          $.ajax({
            url: "{{route('addingredient')}}",
            type: "POST",
            data: { name: name, id_restaurant: id_restaurant },
            success: function (response) {
              if(response.exist==0){
                    $("#submitAddIngredient").attr('disabled', 'disabled');
                    $("#td_ingredients button:last-child").before(`<div class="dd-handle" style="width : 100%;" div-id="`+response.id+`" div-name="`+name+`""><label>${response.name}</label><div class="butttons"><a title="Delete" href="javascript:;" class="ingredient_del"><i class="fa fa-trash"></i></a><a style="margin-left: 7px;" title="Edit" href="javascript:;" data-toggle="modal" data-target="#ModalUpdateIngredient" class="ingredient_upd" id="`+response.id+`"><i class="fa fa-pencil"></i></a></div><input type="hidden" name="supplements[]" value="`+response.id+`"></div>`);
                    $('.butttons').on('click','a.ingredient_del',function(){
                            var element = $(this).parent().parent();
                            var id = $(this).parent().parent().attr('div-id');
                            console.log(id);
                            function ff(x){
                                x.remove();
                            }
                            swal({ 
                                title: "Are you sure ?",
                                text: "You will not be able to recover this record data!",
                                type: "warning",
                                showCancelButton: true,   confirmButtonColor: "#ff0000",
                                confirmButtonText: "Yes!",
                                cancelButtonText: "No",
                                closeOnConfirm: true 
                            },
                            function(){
                              $.ajax({
                                url: "{{route('delingredient')}}",
                                type: "POST",
                                data: { id: id },
                                success: function (response) {
                                  ff(element);
                                },
                              });
                            });
                    });
                    $('.butttons').on('click','a.ingredient_upd',function(){
                        $('#UpdateIngredientNewName').attr('value',$(this).parent().parent().attr('div-name'));
                        $('#UpdateIngredientId').attr('value',$(this).parent().parent().attr('div-id'));
                    });
                    $("#success2").children().text("Ingredient Ajouté")
                    $("#success2").attr("class","alert alert-success");
                    $("#success2").css("display", "block");
                    setTimeout(() => {
                      $("#formAddIngredient").trigger("reset");
                      $("#ModalAddNewIngredient").modal("hide");
                      $("#success2").css("display", "none");
                      $("#submitAddIngredient").removeAttr('disabled');
                    }, 2000);
              }
              else{
                    $("#success2").children().text("Ingredient Existe déja");
                    $("#success2").attr("class","alert alert-danger");
                    $("#success2").css("display", "block");
                    setTimeout(() => {
                        $("#success2").css("display", "none");
                    }, 3000);
                }
            },
          });
        });

        $("#formUpdateIngredient").submit(function (e) {
          e.preventDefault();
          var newname = $("#UpdateIngredientNewName").val();
          var id = $("#UpdateIngredientId").val();
          var id_restaurant = "{{$restaurant->id}}";
          $.ajax({
            url: "{{route('updateingredient')}}",
            type: "POST",
            data: { newname: newname, id: id, id_restaurant: id_restaurant },
            success: function (response) {
              if(response.exist==0){
                    $("#submitUpdateIngredient").attr('disabled', 'disabled');
                    $('#td_ingredients div[div-id="'+id+'"]').children(":first").text(response.name);
                    $('#td_ingredients div[div-id="'+id+'"]').attr('div-name',response.name);
                    $("#success5").children().text("Cet ingredient a été mis à jour");
                    $("#success5").attr("class","alert alert-success");
                    $("#success5").css("display", "block");
                    setTimeout(() => {
                        $("#ModalUpdateIngredient").modal("hide");
                        $("#formUpdateIngredient").trigger("reset");
                        $("#success5").css("display","none");
                        $("#submitUpdateIngredient").removeAttr('disabled');
                    }, 3000);
              }
              else{
                    $("#success5").children().text("Il Existe déja un ingredient identique");
                    $("#success5").attr("class","alert alert-danger");
                    $("#success5").css("display", "block");
                    setTimeout(() => {
                        $("#success5").css("display", "none");
                    }, 3000);
              }
            },
          });
        });

        $('.addoption1').on('click',function(){
          $('#newGroupOptions1').append('<div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptions[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prix[]" value="0" min="0" step="0.1" style="width: 50px;"><button type="button" id="remove_option" class="btn-primary" style="background-color : red; margin-left : 20px;">-</button></div>');
        });

        $('.addoption2').on('click',function(){
          var n = $('#newGroupOptions2').children().length;
          $('#newGroupOptions2').append('<div class="add_option_div"><label style="margin-right : 10px;">Option :</label><input type="text" required name="newoptionsUpdate[]" class="" size="20"><label style="margin-left : 10px;"> Prix :</label><input type="number" required name="prixUpdate[]" id="prix'+n+'" value="0" min="0" step="0.1" style="width: 50px;"><button type="button" id="remove_option" class="btn-primary" style="background-color : red; margin-left : 20px;">-</button></div>');
        });

        $('#newGroupOptions1').on('click','.add_option_div #remove_option',function(){
          $(this).parent().remove();
        });

        $('#newGroupOptions2').on('click','.add_option_div #remove_option',function(){
          $(this).parent().remove();
        });

        $("#formAddOptions").submit(function (e) {
          e.preventDefault();
          var id_restaurant = "{{$restaurant->id}}";
          var groupe_name = $('input[name="newGroupeName"]').val();
          var min = $('input[name="newmin"]').val();
          var max = $('input[name="newmax"]').val();
          var options = [];
          var prix = [];
          var i=0;
          $('input[name="newoptions[]"]').each(function(){
            options[i]=$(this).val();
            i++;
          });
          i=0;
          $('input[name="prix[]"]').each(function(){
            prix[i]=$(this).val();
            i++;
          });
          $.ajax({
            url: "{{route('addoptions')}}",
            type: "POST",
            data: { id_restaurant: id_restaurant , groupe_name: groupe_name, min: min, max : max, options : options, prix: prix },
            success: function (response) {
              if(response.exist==0){
                $("#submitAddOptions").attr('disabled', 'disabled');
                var content=``;
                content += `<div class="option">`;
                content += `<div class="option-container" id="group${response.groupe_id}">`;
                content += `<div class="checkbox-label">`;
                content += `<input  class="checkbox" type="checkbox" value="${response.groupe_id}" id="${response.groupe_id}" name="options_groupe[]"><label>${response.groupe_name}</label>`;
                content += `</div>`;
                content += `<div class="butttons"><a title="Delete" href="javascript:;" class="options_del"><i class="fa fa-trash"></i></a><a style="margin-left: 7px;" title="Edit" href="" data-toggle="modal" data-target="#ModalUpdateOptions" class="options_upd" id="${response.groupe_id}"><i class="fa fa-pencil"></i></a></div>`;
                content += `</div>`;
                content += `<div class="option-group" style="display:block;">`;
                content += `<div>`;
                content += `<label>Min : </label><input type="number" value="${response.min}" min="0" name="" id="min${response.groupe_id}" style="width: 37px;"><label>  Max : </label><input type="number" value="${response.max}" min="1"  name="" id="max${response.groupe_id}" style="width: 37px;">`;
                content += `</div>`;
                for(var i=0;i<response.options_id.length;i++){
                  content += `<div class="options"><div><input type="checkbox" value="${response.options_id[i]}" name="" class="custom-checkbox option${response.groupe_id}"><label>${response.options_name[i]}</label></div><div>  Prix : ${response.options_prix[i]}</div></div>`;
                }
                content += '</div>';
                content += '</div>';
                $("#td_options button:last-child").before(content);
                $("#success3").children().text("Options Ajoutées")
                $("#success3").attr("class","alert alert-success");
                $("#success3").css("display", "block");
                setTimeout(() => {
                  $("#formAddOptions").trigger("reset");
                  $("#ModalAddNewOptions").modal("hide");
                  $("#success3").css("display", "none");
                  $("#submitAddOptions").removeAttr('disabled');
                }, 2000);
              }
              else{
                $("#success3").children().text("Options Existent déja");
                $("#success3").attr("class","alert alert-danger");
                $("#success3").css("display", "block");
                setTimeout(() => {
                    $("#success3").css("display", "none");
                }, 3000);
              }
            },
          });
        });

        $("#formUpdateOptions").submit(function (e) {
          e.preventDefault();
          var id_groupe = $('input[name="idGroupeUpdate"]').val();
          var new_groupe_name = $('input[name="newGroupeNameUpdate"]').val();
          var old_groupe_name = $('input[name="oldGroupeNameUpdate"]').val();
          var min = $('input[name="newminUpdate"]').val();
          var max = $('input[name="newmaxUpdate"]').val();
          var options_name = [];
          var prix = [];
          var i=0;
          $('input[name="newoptionsUpdate[]"]').each(function(){
            options_name[i]=$(this).val();
            i++;
          });
          i=0;
          $('input[name="prixUpdate[]"]').each(function(){
            prix[i]=$(this).val();
            i++;
          });
          $.ajax({
            url: "{{route('updateOptions')}}",
            type: "POST",
            data: {idGroupeUpdate: id_groupe, newGroupeNameUpdate: new_groupe_name, oldGroupeNameUpdate: old_groupe_name, newminUpdate: min, newmaxUpdate: max, newoptionsUpdate: options_name, prixUpdate: prix},
            success: function (response) {
              if(response.exist==0){
                $("#submitUpdateOptions").attr('disabled', 'disabled');
                $('#group'+id_groupe).children().eq(0).children().eq(1).text(new_groupe_name);
                $('#group'+id_groupe).parent().children().eq(1).children().eq(0).children().eq(1).attr('value',min);
                $('#group'+id_groupe).parent().children().eq(1).children().eq(1).children().eq(1).attr('value',max);
                var n= $('#group'+id_groupe).parent().children().eq(1).children().length;
                for(var i=1;i<n;i++){
                  $('#group'+id_groupe).parent().children().eq(1).children().eq(1).remove();
                }
                for(var i=0;i<options_name.length;i++){
                  $('#group'+id_groupe).parent().children().eq(1).append(`<div class="options"><div><input type="checkbox" value="" name="" class="custom-checkbox" checked><label>`+options_name[i]+`</label></div><div>  Prix : `+prix[i]+`</div></div>`);
                }
                $("#success6").children().text("Options Modifiés")
                $("#success6").attr("class","alert alert-success");
                $("#success6").css("display", "block");
                setTimeout(() => {
                  $("#formUpdateOptions").trigger("reset");
                  $("#ModalUpdateOptions").modal("hide");
                  $("#success6").css("display", "none");
                  $("#submitUpdateOptions").removeAttr('disabled');
                }, 2000);
              }
              else{
                $("#success6").children().text("Options Existent déja");
                $("#success6").attr("class","alert alert-danger");
                $("#success6").css("display", "block");
                setTimeout(() => {
                    $("#success6").css("display", "none");
                }, 3000);
              }
            },
          });
        });
    });
    
</script>
@endpush

