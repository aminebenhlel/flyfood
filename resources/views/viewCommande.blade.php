@extends('crudbooster::admin_template')
@push('head')
<style>
    .head{
        display: flex; 
        margin-bottom: 30px;
        font-size: 18px;
        justify-content: space-between;
    } 
    .head_first_span{
        margin-right: 10px;
        font-weight: bold;
        display: inline-block;
        padding: 5px;
        margin: 0 15px;
        min-width: 100px;
        font-size: 16px;
        text-align: right;
    }
    .user_info{
        display: flex;
        justify-content: space-between;
        min-width: 350px;
    }

    .commande_info{
        display: flex;
        justify-content: space-between;
        flex-direction: column;
    }

    .mid{
        margin-bottom: 30px;
    }
    .resto{
        border-radius: 5px;
        padding: 18px;
        margin: 23px 3px;
        box-shadow: rgb(99 99 99 / 40%) 0px 2px 8px 0px;
        padding-bottom: 26px;
        font-size: 17px;
    }
    .resto_head{
        text-align: center;
        border-bottom: 1px solid;
        padding: 12px;
        margin-bottom: 23px;
    }
    .label_resto{
        font-weight: bold;
        margin-right: 8px;
        font-size: 20px;
    }
    .produit{
        display: flex;
        flex-direction: column;
    }
    .table_tr{
        display: flex;
        flex-direction: row;
        border: 1px solid #ddd;
        border-top: none;
    }

    .header {
        font-weight: bold;
        background: #ccc;
        padding: 10px;
    }   
    .table_tr div{
        width: 150px
    }

    .composants{
        display: flex;
        flex-direction: column;
    }

    .butttons{
        display: flex;
        justify-content: space-between;
        width: 400px;
        padding: 0px 18px;
    }
    .button{
        border-radius: 5px;
        width: 110px;
        height: 33px;
    }
    .backGround:nth-child(even){
        background: #f5f5f5;
    }
    .backGround{
        padding: 5px;
    }
    .head img{
        width:140px;
    }
    .price{
        font-size: 18px; 
    }

    .livreur_label {
        font-size: 16px;
        cursor: pointer;
    }

    .livreur_checkbox {
        margin-top: 9px !important;
        width: 20px;
        height: 20px;
        cursor: pointer;
    }
</style>
@endpush
@section('content')
    <div>
        <p><a title="Return" href="{{ URL::asset('index.php/admin/commande') }}"><i
                    class="fa fa-chevron-circle-left "></i>&nbsp; Back to orders list</a></p>
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><i class="fa fa-glass"></i> Gestion du commande n° : {{ $commande->id }}</strong>
            </div>
            <div class="panel-body" style="padding:20px 0px 0px 0px">
                    <div class="box-body" id="parent-form-area">
                        <div class="table-responsive">
                            <table id="table-detail" class="table table-striped">
                                <div class="head">
                                    <div class="user_info">
                                        <img src="https://thumbs.dreamstime.com/b/default-avatar-profile-flat-icon-social-media-user-vector-portrait-unknown-human-image-default-avatar-profile-flat-icon-184330869.jpg" alt="">
                                        <div>
                                            <div><span class="head_first_span">Client : </span><span>{{$commande->client_name}}</span></div>
                                            <div><span class="head_first_span">Phone : </span><span>{{$commande->client_phone}}</span></div>
                                            <div><span class="head_first_span">Adress : </span><span>{{$commande->adresse}}</span></div>
                                        </div>
                                    </div>
                                    <div class="commande_info">
                                        <div class="commande_info">
                                            <div><span class="head_first_span">Status : </span><span>{{$commande->status}}</span></div>
                                            <div><span class="head_first_span">Total : </span><span>{{$commande->prix}} Dt</span></div>
                                            <div><span class="head_first_span">Ordered at : </span><span>{{$commande->created_at}}</span></div>
                                        </div>
                                        <div class="butttons">
                                            <div class="confirm">
                                                <button type="button" class="btn btn-primary" id="confirm" data-toggle="modal" data-target="#choose_livreur"
                                                @if($commande->status != 'In progress') disabled @endif>Confirm <span class="fa fa-check"></span></button>
                                            </div>
                                            <div class="cancel">
                                                <button type="button" class="btn btn-danger" id="cancel" 
                                                @if($commande->status == 'Ordred' || $commande->status == 'Canceled') disabled @endif>Cancel <span class="fa fa-times"></span></button>
                                            </div>
                                            <div class="ordred">
                                                <button type="button" class="btn btn-success" id="ordred"
                                                @if($commande->status != 'Confirmed') disabled @endif>Delivered <span class="fa fa-check-circle"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mid">
                                    @foreach ($restaurants as $name => $restaurant)
                                        <div class="resto">
                                            <div class="resto_head">
                                                <span class="label_resto">Restaurant : </span>
                                                <span>{{$name}}</span>
                                            </div>
                                            
                                                <div class="produit">
                                                    <div class="table_tr header">
                                                        <div class="">Product</div>
                                                        <div class="">Qunatity</div>
                                                        <div class="">Total</div>
                                                        <div class="">Without</div>
                                                        <div class="">Supplements</div>
                                                        <div class="">Options</div>
                                                    </div>
                                                    @foreach ($restaurant as $produit)
                                                        <div class="table_tr backGround">
                                                            <div class="">{{$produit->name}}</div>
                                                            <div class="">{{$produit->quantite}}</div>
                                                            <div class="price">{{$produit->prix}} DT</div>
                                                            <div class="composants">
                                                                @foreach ($produit->ingredients as $item)
                                                                    <span>{{$item}}</span>
                                                                @endforeach
                                                            </div>
                                                            <div class="composants">
                                                                @foreach ($produit->supplements as $item)
                                                                    <span>{{$item->name}}</span>
                                                                @endforeach
                                                            </div>
                                                            <div class="composants">
                                                                @foreach ($produit->options as $item)
                                                                    <div>- {{$item->name}}</div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                        </div>
                                    @endforeach
                                </div>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <!-- Modal Select livreur  -->

    <div class="modal fade" id="choose_livreur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <strong><i class="fa fa-glass"></i> Choose delivery agent</strong>
                </div>
                <div class="panel-body" style="padding:20px 0px 0px 0px">
                  <form class="form-horizontal" method="post" id="formAddSupplement" enctype="multipart/form-data" action="{{route('confirm_order', $commande->id)}}" >
                    @csrf
                    <div class="box-body" id="parent-form-area">  
                      <div class="form-group header-group-0 " id="form-group-prix" style="display: flex;
                      flex-direction: column;">
                        @foreach ($livreurs as $livreur)
                        <div>
                            <label for="livreur{{$livreur->id}}" class="control-label col-sm-2 livreur_label">
                                {{$livreur->name}}
                            </label>
                            <input class="livreur_checkbox" id="livreur{{$livreur->id}}" type="radio" name="livreur" value="{{$livreur->id}}">
                        </div>
                        @endforeach
                      </div>           
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="background: #F5F5F5;">
                      <div class="form-group">
                        <label class="control-label col-sm-2"></label>
                          <div class="col-sm-10">
                            <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success" id="submitAddSupplement">Confirm</button>
                          </div>
                      </div>
                    </div>
                    <!-- /.box-footer-->
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

@endsection
@push('bottom')
<script>
    $("#cancel").on('click',function (e) {
        $.ajax({
        url: "{{route('cancel_order', $commande->id)}}",
        type: "POST",
        data: { },
        success: function (response) {
            window.location.replace("{{ URL::asset('index.php/admin/commande/detail/'.$commande->id)}}");
        },
        });
    });
    $("#ordred").on('click',function (e) {
        $.ajax({
        url: "{{route('set_order_ordred', $commande->id)}}",
        type: "POST",
        data: { },
        success: function (response) {
            window.location.replace("{{ URL::asset('index.php/admin/commande/detail/'.$commande->id)}}");
        },
        });
    });
    // $("#confirm").on('click',function (e) {
    //     $.ajax({
    //     url: "{{route('confirm_order', $commande->id)}}",
    //     type: "POST",
    //     data: { },
    //     success: function (response) {
    //         window.location.replace("{{ URL::asset('index.php/admin/commande/detail/'.$commande->id)}}");
    //     },
    //     });
    // });
</script>
@endpush
